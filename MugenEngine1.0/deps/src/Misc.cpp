//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#include "Misc.h"

namespace Misc
{
    VAR sign(VAR value)
    {
        return (value > 0) - (value < 0);
    }


    int random(int beginRange, int endRange)
    {
        return beginRange + (rand() % (int)(endRange-beginRange+1));
    }
    int random(std::string str, std::string delimiter)
    {
        std::string strMini = str.substr(0,str.find(delimiter));
        str.erase(0,str.find(delimiter)+delimiter.length());
        std::string strMaxi = str.substr(0,str.find(delimiter));

        int mini = std::stoi(strMini);
        int maxi = std::stoi(strMaxi);

        return random(mini,maxi);
    }
    int getRandom(std::string str, std::string delimiter)
    {
        int random = 0;
        if (str.find(delimiter) != std::string::npos)
        {
            random = Misc::random(str,delimiter);
        }
        else
        {
            random = std::stoi(str);
        }

        return random;
    }


    VAR pourcent(VAR maxValue, VAR value)
    {
        return (100*value)/maxValue;
    }
    VAR proportion(VAR maxValue, VAR value, VAR range)
    {
        return (range*value)/maxValue;
    }

    void BoolArray::set(int index, int value)
    {
        _data = _data|(value<<index);
    }

    void BoolArray::on(int index)
    {
        _data = _data|(1<<index);
    }

    void BoolArray::off(int index)
    {
        _data = _data|(0<<index);
    }


    bool BoolArray::get(int index)
    {
        return _data&(1<<index);
    }

    void BoolArray::flip(int index)
    {
        _data = _data^(1<<index);
    }

    void BoolArray::reset()
    {
        _data = 0;
    }



}
