#include "Clip.h"

bool Clip::_showClipInfo = false;

Clip* Clip::getMaster(Clip* clip)
{
    Clip* master = nullptr;

    if (nullptr != clip->_original)
        master = getMaster(clip->_original);
    else
        master = clip;

    return master;
}


void Clip::update()
{
    updateClipRect();

    // Order is VERY IMPORTANT for avoid SEG FAULT

    // (1) update CLIP
    if (_update != nullptr && _isActive)
        (*_update)(this);
    else
        mlog("- " + _name + " update() not defined !\n");

    // Check if have SUB CLIP !
    if (numActiveObject() > 0 && _isActive)
    {
        // (2) update SUB CLIP
        if (!_vecObject.empty())
        {
            for (auto& it: _vecObject)
            {
                if (it != nullptr)
                    it->update();

            }

//            auto it = _vecObject.begin();
//            while (it != _vecObject.end())
//            {
//                if ((*it) != nullptr)
//                    (*it)->update();
//                ++it;
//            }

        }
    }
    _message = nullptr;

}

void Clip::render()
{
    if (_render != nullptr && _isVisible)
        (*_render)(this);
    else
        mlog("- " + _name + " render() not defined !\n");


    // Check if have SUB CLIP !
    if (numActiveObject() > 0 && _isVisible)
    {
        // (3) sort SUB CLIP by Z order
        sortZIndex(_vecObject);//, _vecZIndex);

        if (!_vecObject.empty())
        {
            for (unsigned index = 0; index < _vecObject.size(); ++index)
            {
                if (index < _vecZIndex.size())
                {
                    if (zIndex(index) >= 0 &&
                        zIndex(index) < _vecObject.size())
                    {
                        if (_vecObject[zIndex(index)] != nullptr)
                                _vecObject[zIndex(index)]->render();
                    }
                }
            }
        }
    }

    // (2) render SUB CLIP
//        if (!_vecObject.empty())
//        {
//            for (auto & it: _vecObject)
//            {
//                if (it != nullptr)
//                        it->render();
//
//            }
//        }
}

void Clip::showClipInfo()
{
    Clip::_showClipInfo = true;
}
void Clip::hideClipInfo()
{
    Clip::_showClipInfo = false;
}

Clip* Clip::cloneOf(Clip* original, std::string name)
{

    if (nullptr != original)
    {
        Clip *clone = new Clip(*original);

        clone->_original = original;
        clone->_master = Clip::getMaster(clone);

        if (name == "")
            clone->_name = original->_name;
        else
            clone->_name = name;

        //mlog ("--- Begin deep copy ---\n");

        clone->_isActive = original->_isActive;

        clone->_mapComponent.clear();

        if (!original->_mapComponent.empty())
        {
            std::map<int,Component::Base*>::const_iterator it = original->_mapComponent.begin();

            while (it != original->_mapComponent.end())
            {
                //clone->_mapComponent[it->first] = new Component(*(it->second));
                clone->_mapComponent[it->first] = (*(it->second)).clone();
                // Important Clone Component attached to Clone Clip
                clone->_mapComponent[it->first]->_clip = clone;
                ++it;
            }
        }
        else
        {
            mlog("Deep Copy Component Failed : "+original->_name+" -> "+clone->_name +" \n");
        }

        clone->_mapCollideZone.clear();

        if (!original->_mapCollideZone.empty())
        {
            std::map<int,Collide::Zone*>::const_iterator it = original->_mapCollideZone.begin();

            while (it != original->_mapCollideZone.end())
            {
                //clone->_mapComponent[it->first] = new Component(*(it->second));
                clone->_mapCollideZone[it->first] = new Collide::Zone(*(it->second));
                // Important Clone Component attached to Clone Clip
                clone->_mapCollideZone[it->first]->_clip = clone;
                ++it;
            }

        }
        else
        {
            mlog("Deep Copy Collide::Zone Failed : "+original->_name+" -> "+clone->_name +" \n");
        }

        return clone;
    }
    else
    {
        mlog("cloneOf error : original don't exist ! \n");
        return nullptr;
    }


}

//Clip* Clip::setup(
//    std::shared_ptr<Window> window,
//    ALLEGRO_FONT *font,
//    Input::Mouse* mouse,
//    bool isActive)
//{
//    _window = window;
//    _font = font;
//    _mouse = mouse;
//    _isActive = isActive;
//
//    return this;
//}

Clip::Clip(std::string const& name):
    _name(name)
{
    _mapComponent.clear();
    _mapCollideZone.clear();

    if (nullptr != _parent)
    {
//        _window    = _parent->_window;
//        _font      = _parent->_font;
        _isActive  = _parent->_isActive;
    }

    //mlog("- Clip created !\n");
}

Clip::~Clip()
{
    if (nullptr != _done)
        (*_done)(this);

    //std::map<int,Component*>::iterator it = _mapComponent.begin();
    if (!_mapComponent.empty())
    {
        auto it = _mapComponent.begin();
        while (it != _mapComponent.end())
        {
            if (nullptr != it->second)
            {
                //mlog ("- Delete Component:"+ std::to_string(it->second->_type) + " of Clip: " + _name + "\n");
                delete it->second;
                it->second = nullptr;
            }
            ++it;
        }
        _mapComponent.clear();
    }

    _mapNumber.clear();
    _mapString.clear();

    // Delete all Collide::Zone if exist !


    if (!_mapCollideZone.empty())
    {
        auto it = _mapCollideZone.begin();

        while(it != _mapCollideZone.end())
        {
            if (nullptr != it->second)
            {
                delete it->second;
                it->second = nullptr;
            }
            ++it;
        }
        _mapCollideZone.clear();
    }

    //mlog("- Delete Clip: " + _name + "\n");

    //printf("- Delete Clip: %s \n" , _name.c_str());
}

Clip* Clip::setPosition(int x, int y, int z)
{
    _x = x;
    _y = y;
    _z = z;

    updateClipRect();

    return this;
}

Clip* Clip::parent()
{
    return _parent;
}

Clip* Clip::setParent(Clip* parent)
{
    _parent = parent;
    return this;
}
Clip* Clip::appendTo(Clip* parent)
{
    if (nullptr != parent)
    {
        _isAppend = true;
        setParent(parent);
        _parent->add(this);
    }
    if (nullptr != _init)
        (*_init)(this);

    return this;
}
//Clip* Clip::setupParent()
//{
//    if (nullptr != _parent)
//    {
//        this->setup
//        (
//            _parent->_window,
//            _parent->_font,
//            _parent->_mouse
//        );
//    }
//    return this;
//}

Clip* Clip::moveTo(Clip* newParent)
{
    if (nullptr != newParent)
    {
        Clip *oldParent = this->_parent;
        cloneOf(this,this->name())
        ->appendTo(newParent);
        oldParent->del(this->id());
    }
    return this;
}

Clip* Clip::attach(Component::Base* component)
{
    component->_clip = this;
    _mapComponent[component->_type] = component;
    return this;
}

Clip* Clip::detach(int type)
{
    if (_mapComponent[type] != nullptr)
        delete _mapComponent[type];
    _mapComponent.erase(type);
    return this;
}

Clip* Clip::setCollideZone(int index, Rect const& rect)
{


    Collide::Zone* collideZone = new Collide::Zone
    (
        index,
        Rect
        {
            absX() + rect._x,
            absY() + rect._y,
            rect._w,
            rect._h
        },
        this
    );
    _mapCollideZone[index] = collideZone;

    return this;
}
Collide::Zone* Clip::getCollideZone(int index)
{
    if (nullptr != _mapCollideZone[index])
        return _mapCollideZone[index];

    return nullptr;
}

Clip* Clip::updateCollideZone(int index, Rect const& rect)
{
    if (nullptr != getCollideZone(index))
        getCollideZone(index)->_rect = rect;

    return this;
}
Clip* Clip::renderCollideZone(int index, Draw::Color const& color)
{
    if (nullptr != getCollideZone(index) && Clip::_showClipInfo)
        Draw::rect(getCollideZone(index)->_rect, color,0);

    return this;
}



void Clip::updateClipRect()
{
    // For determinate relative & absolute Clip position !
    if (nullptr != _parent)
    {
        _rect._x = _x - _oX + _parent->_rect._x;
        _rect._y = _y - _oY + _parent->_rect._y;
    }
    else
    {
        _rect._x = _x - _oX;
        _rect._y = _y - _oY;
    }

//    for (auto it: _mapCollideZone)
//    {
//        if (nullptr != it.second)
//        {
//            it.second->_rect._x = _rect._x + it.second->_rect._x;
//            it.second->_rect._y = _rect._y + it.second->_rect._x;
//        }
//    }

}

Clip* Clip::setX(VAR x)
{
    _x = x;
    updateClipRect();
    return this;
}

Clip* Clip::setY(VAR y)
{
    _y = y;
    updateClipRect();
    return this;
}

Clip* Clip::setZ(VAR z)
{
    _z = z;
    return this;
}

Clip* Clip::setPivotX(VAR x)
{
    _oX = x;
    updateClipRect();
    return this;
}

Clip* Clip::setPivotY(VAR y)
{
    _oY = y;
    updateClipRect();
    return this;
}

Clip* Clip::setPivot(VAR x, VAR y)
{
    setPivotX(x);
    setPivotY(y);
    return this;
}

Clip* Clip::setPivot(int pivot)
{
    VAR w = _rect._w;
    VAR h = _rect._h;

    switch (pivot)
    {

        case NW:
            setPivot(0,0);
            break;

        case NE:
            setPivot(w,0);
            break;

        case SW:
            setPivot(0,h);
            break;

        case SE:
            setPivot(w,h);
            break;

        case NM:
            setPivot(w/2,0);
            break;

        case SM:
            setPivot(w/2,h);
            break;

        case WM:
            setPivot(0,h/2);
            break;

        case EM:
            setPivot(w,h/2);
            break;

        case M:
            setPivot(w/2,h/2);
            break;
    }

    return this;
}


Clip* Clip::showRect(Draw::Color const& color)
{
    if (Clip::_showClipInfo)
    // Draw the Clip rect
    al_draw_rectangle
    (
        .5+_rect._x, .5+_rect._y,
        .5+_rect._x+_rect._w-1, .5+_rect._y+_rect._h-1,
        color,0
    );
    return this;
}

Clip* Clip::showPivot(Draw::Color const& color, float r)
{
    if (Clip::_showClipInfo)
     // Draw the Pivot
    al_draw_filled_circle
    (
        .5+_rect._x+_oX, .5+_rect._y+_oY, .5+r,
        color
    );
    return this;
}

Clip* Clip::showInfo(Draw::Font* font, Draw::Color const& color)
{
    if (Clip::_showClipInfo)
    // Draw the Name a Top Left of rect
    if (nullptr != font)
        al_draw_textf
        (
            font,
            color,
            _rect._x, _rect._y - 14,
            0,
            "%s : Abs = %.2f,%.2f : Rel = %.2f,%.2f : Frame = %i ",
            _name.c_str(),
            absX(), absY(),
            _x, _y,
            _currentFrame
        );
    return this;
}

Clip* Clip::showComponent(Draw::Font* font, VAR x, VAR y, Draw::Color const& color)
{
    if (Clip::_showClipInfo)
        if (!_mapComponent.empty())
        {
            auto it = _mapComponent.begin();

            int index = 0;
            while(it != _mapComponent.end())
            {
                if (nullptr != it->second)
                {

                    std::string componentName = "NOTHING";

//                    auto itComponentName = Component::_mapType.find(it->second->_type);
//                    componentName = (*itComponentName);

                    for (auto& itComponent: Component::_mapType)
                    {
                        if (itComponent.second == it->second->_type)
                            componentName = itComponent.first;
                    }


                    al_draw_textf
                    (
                        font,
                        color,
                        x, y+index*al_get_font_line_height(font),
                        0,
                        "%i : %s",
                        index,
                        componentName.c_str()
                    );
                }
                ++it;
                ++index;
            }
        }

    return this;
}


Clip* Clip::setNumber(std::string const& name, int const& value)
{
    _mapNumber[name] = value;
    return this;
}
Clip* Clip::setString(std::string const& name, std::string const& value)
{
    _mapString[name] = value;
    return this;
}

Clip* Clip::setClip(std::string const& name, Clip* clip)
{
    _mapClip[name] = clip;
    return this;
}



Clip* Clip::setFunction(int functionId, std::function<void(Clip*)> const& functionDef)
{
    _mapFunction[functionId] = std::make_shared<std::function<void(Clip*)>>(functionDef);
    return this;
}
Clip* Clip::setUpdate(std::function<void(Clip*)> const& update)
{
    _update = std::make_shared<std::function<void(Clip*)>>(update);
    return this;
}
Clip* Clip::setRender(std::function<void(Clip*)> const& render)
{
    _render = std::make_shared<std::function<void(Clip*)>>(render);
    return this;
}
Clip* Clip::setInit(std::function<void(Clip*)> const& init)
{
    _init = std::make_shared<std::function<void(Clip*)>>(init);
    return this;
}
Clip* Clip::setDone(std::function<void(Clip*)> const& done)
{
    _done = std::make_shared<std::function<void(Clip*)>>(done);
    return this;
}

void Clip::runMapFunction(int functionId, Clip* playable)
{
    (*_mapFunction[functionId])(playable);
}
void Clip::runUpdate(Clip* playable)
{
    (*_update)(playable);
}
void Clip::runRender(Clip* playable)
{
    (*_render)(playable);
}
void Clip::runInit(Clip* playable)
{
    (*_init)(playable);
}
void Clip::runDone(Clip* playable)
{
    (*_done)(playable);
}


Clip* Clip::setCollidable(bool isCollidable)
{
    _isCollidable = isCollidable;
    return this;
}

Clip* Clip::setCameraMoveFactor(float cameraMoveFactor)
{
    _cameraMoveFactor = cameraMoveFactor;
    return this;
}

Clip* Clip::setSize(int w, int h)
{
    _rect._w = w;
    _rect._h = h;
    return this;
}

Clip* Clip::setActive(bool isActive)
{
    _isActive = isActive;

    if (_isActive)
        _currentFrame = 0;

    return this;
}

Clip* Clip::setVisible(bool isVisible)
{
    _isVisible = isVisible;

    return this;
}
Clip* Clip::setMouse(Input::Mouse* mouse)
{
    _mouse = mouse;
    return this;
}
Clip* Clip::setId(int id)
{
    _id = id;
    return this;
}
int Clip::id()
{
    return _id;
}
std::string Clip::name() const
{
    return _name;
}
Clip* Clip::setType(int type)
{
    _type = type;
    return this;
}
int Clip::type()
{
    return _type;
}
void Clip::hide()
{
    _isVisible = false;
}
void Clip::show()
{
    _isVisible = true;
}

// Player
bool Clip::isPlay() const
{
    return _isPlay;
}

void Clip::play()
{
    _isPlay = true;
}
void Clip::stop()
{
    _isPlay = false;
    _currentFrame = 0;
}

void Clip::pause()
{
    _isPlay = false;
}

void Clip::resume()
{
    play();
}

void Clip::start()
{
    _currentFrame = 0;
    play();
}

void Clip::playAt(int frame)
{
    _currentFrame = frame;
    _isPlay = true;
}

void Clip::stopAt(int frame)
{
    _currentFrame = frame;
    _isPlay = false;
}

bool Clip::onFrame(int frame)
{
    if (_currentFrame == frame)
        return true;
    else
        return false;
}

void Clip::prevFrame()
{
    if (_isPlay)
        --_currentFrame;
}

void Clip::nextFrame()
{
    if (_isPlay)
        ++_currentFrame;
}

Rect Clip::absRect()
{
    return Rect{absX(),absY(),_rect._w,_rect._h};
}

VAR Clip::absX()
{
    if (nullptr != _parent)
        return _parent->_rect._x + _x - _oX;
    else
        return _x;
}
VAR Clip::absY()
{
    if (nullptr != _parent)
        return _parent->_rect._y + _y - _oY;
    else
        return _y;
}

VAR Clip::parentX()
{
    if (nullptr != _parent)
        return _parent->_x;
    else
        return 0;
}

VAR Clip::parentY()
{
    if (nullptr != _parent)
        return _parent->_y;
    else
        return 0;
}

VAR Clip::parentRectX()
{
    if (nullptr != _parent)
        return _parent->_rect._x;
    else
        return 0;
}

VAR Clip::parentRectY()
{
    if (nullptr != _parent)
        return _parent->_rect._y;
    else
        return 0;
}

VAR Clip::parentRectW()
{
    if (nullptr != _parent)
        return _parent->_rect._w;
    else
        return 0;
}

VAR Clip::parentRectH()
{
    if (nullptr != _parent)
        return _parent->_rect._h;
    else
        return 0;
}
