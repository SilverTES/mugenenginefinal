#include "BaseComponent.h"
#include "Draw.h"

namespace Component
{
//{ Skin

    Clip* Skin::setId(int id)
    {
        _id = id;
        return _clip;
    }
    Clip* Skin::setColor(RGBA const& color)
    {
        _color = color;
        return _clip;
    }
    Clip* Skin::setRGB(unsigned char r, unsigned char g, unsigned char b)
    {
        _color._r = r;
        _color._g = g;
        _color._b = b;
        return _clip;
    }
    Clip* Skin::setAlpha(unsigned char alpha)
    {
        _color._a = alpha;
        return _clip;
    }
    Clip* Skin::setTileSize(VAR tileW, VAR tileH)
    {
        _tileW = tileW;
        _tileH = tileH;
        return _clip;
    }
//}

//{ Gui
//}

//{ Command

    Command::~Command()
    {
        Misc::kill(_player);
    }

    Clip* Command::setPlayer(Player* player)
    {
        if (nullptr != player)
            _player = player;
        else
            mlog("Command::setPlayer error : invalid player == nullptr !\n");

        return _clip;
    }
    Clip* Command::setKeyState(ALLEGRO_KEYBOARD_STATE* keyState)
    {
        if (nullptr != keyState)
        {
            if (nullptr != _player)
                _player->getController()->setKeyState(keyState);
        }
        else
            mlog("Command::setKeyState error : invalid keyState == nullptr !\n");

        return _clip;
    }

    Clip* Command::setButton(int id, int idKey, int idJoy, int idStick, int idAxis, float idDirection, int idButton)
    {
        if (nullptr != _player)
            _player->getController()->setButton(id, idKey, idJoy, idStick, idAxis, idDirection, idButton);
        return _clip;
    }
    Clip* Command::setButton(int id, std::shared_ptr<Button>button)
    {
        if (nullptr != _player)
            _player->getController()->setButton(id, button);
        return _clip;
    }
    Clip* Command::loadController(Json const& controller)
    {
        if (nullptr != controller &&
            nullptr != _player)
        {
            for (auto& it: controller)
            {
                if (nullptr != it)
                {
                    std::string strId = it[0];
                    int id  = Controller::SNESButtonDicoMap[strId];
                    int idKey = it[1];
                    int idJoy = it[2];
                    int idStick = it[3];
                    int idAxis = it[4];
                    int idDirection = it[5];
                    int idButton = it[6];

                    _player->getController()->setButton(id, idKey, idJoy, idStick, idAxis, idDirection, idButton);
                }
            }
        }

        return _clip;
    }
    int Command::onButton(int id)
    {
        if (nullptr != _player)
            return _player->getController()->getButton(id);
        else
            return 0;
    }
//}

//{ Line

    Clip* Line::setLine(VAR x1, VAR y1, VAR x2, VAR y2, Draw::Color color, VAR thickness)
    {
        _x1 = x1;
        _y1 = y1;
        _x2 = x2;
        _y2 = y2;
        _color = color;
        _thickness = thickness;

        return _clip;
    }
//}

//{ Loop

    Clip* Loop::setLoop(int current, int begin, int end, int direction, int loopType)
    {
        _current = current;
        _begin = begin;
        _end = end;
        _direction = direction;
        _loopType = loopType;

        return _clip;
    }
    Clip* Loop::start()
    {
        _run = true;
        return _clip;
    }
    Clip* Loop::stop()
    {
        _run = false;
        return _clip;
    }
    void Loop::update()
    {
        if (_run)
        {
            _current += _direction;

            _atEnd = false;
            _atBegin = false;

            if (_direction > 0 && _current >= _end)
            {
                _atEnd = true;
            }

            if (_direction < 0 && _current <= _begin)
            {
                _atBegin = true;
            }

            switch (_loopType)
            {
                case LOOP_ONCE:
                {
                    if (_atEnd || _atBegin)
                        stop();
                }
                break;

                case LOOP_REPEAT:
                {
                    if (_atEnd)
                        _current = _begin;
                    if (_atBegin)
                        _current = _end;
                }
                break;

                case LOOP_PINGPONG:
                {
                    if (_atEnd || _atBegin)
                        _direction = -_direction;
                }
                break;
                }


        }
    }
//}

//{ Velocity

    Clip* Velocity::setVecX(VAR vecX)
    {
        _vecX = vecX;
        return _clip;
    }

    Clip* Velocity::setVecY(VAR vecY)
    {
        _vecY = vecY;
        return _clip;
    }


    Clip* Velocity::setVX(VAR vx)
    {
        _vx = vx;
        return _clip;
    }
    Clip* Velocity::setVY(VAR vy)
    {
        _vy = vy;
        return _clip;
    }
    Clip* Velocity::setVZ(VAR vz)
    {
        _vz = vz;
        return _clip;
    }

    Clip* Velocity::setVXMax(VAR vxMax)
    {
        _vxMax = vxMax;
        return _clip;
    }
    Clip* Velocity::setVYMax(VAR vyMax)
    {
        _vyMax = vyMax;
        return _clip;
    }
    Clip* Velocity::setVZMax(VAR vzMax)
    {
        _vzMax = vzMax;
        return _clip;
    }

    Clip* Velocity::setAX(VAR ax)
    {
        _ax = ax;
        return _clip;
    }
    Clip* Velocity::setAY(VAR ay)
    {
        _ay = ay;
        return _clip;
    }
    Clip* Velocity::setAZ(VAR az)
    {
        _az = az;
        return _clip;
    }

    Clip* Velocity::setVAX(VAR vax)
    {
        _vax = vax;
        return _clip;
    }
    Clip* Velocity::setVAY(VAR vay)
    {
        _vay = vay;
        return _clip;
    }
    Clip* Velocity::setVAZ(VAR vaz)
    {
        _vaz = vaz;
        return _clip;
    }
//}

//{ Tempo

    Clip* Tempo::setTempo(std::string name, int duration)
    {
        _mapTempo[name] = new Tic{false,false,0,duration};
        return _clip;
    }
    Clip* Tempo::start(std::string name, int duration)
    {
        if (nullptr != _mapTempo[name])
        {
            _mapTempo[name]->_isCount = true;
            _mapTempo[name]->_tic = false;
            _mapTempo[name]->_currentTempo = 0;

            if (duration > 0)
                _mapTempo[name]->_duration = duration;
        }

        return _clip;
    }
    Clip* Tempo::startAt(std::string name, int tempo)
    {
        if (nullptr != _mapTempo[name])
        {
            _mapTempo[name]->_isCount = true;
            _mapTempo[name]->_tic = false;
            _mapTempo[name]->_currentTempo = tempo;
        }
        return _clip;
    }
    Clip* Tempo::pause(std::string name)
    {
        if (nullptr != _mapTempo[name])
            _mapTempo[name]->_isCount = false;
        return _clip;
    }
    Clip* Tempo::stop(std::string name)
    {
        if (nullptr != _mapTempo[name])
        {
            _mapTempo[name]->_isCount = false;
            _mapTempo[name]->_tic = false;
            _mapTempo[name]->_currentTempo = 0;
        }
        return _clip;
    }
    bool Tempo::iscount(std::string name)
    {
        if (nullptr != _mapTempo[name])
            return _mapTempo[name]->_isCount;
        return false;
    }
    bool Tempo::tic(std::string name)
    {
        if (nullptr != _mapTempo[name])
            return _mapTempo[name]->_tic;
        return false;
    }
    int Tempo::current(std::string name)
    {
        if (nullptr != _mapTempo[name])
            return _mapTempo[name]->_currentTempo;
        return 0;
    }
    int Tempo::duration(std::string name)
    {
        if (nullptr != _mapTempo[name])
            return _mapTempo[name]->_duration;
        return 0;
    }
    void Tempo::update()
    {
        for (auto& it:_mapTempo)
        {
            if (nullptr != it.second)
            {
                if (it.second->_isCount)
                    it.second->_currentTempo += 1;

                if (it.second->_currentTempo > it.second->_duration)
                    it.second->_tic = true;
            }

        }
    }
//}

//{ Animate

    void Animate::update()
    {
        //mlog("--- Begin Animate Update ---\n");
        if (_isPlay)
        {
            if (nullptr != _sequence)
                _frameDelay = _sequence->frame(_currentFrame)->_delay;

            if (_countPlayDelay > _playDelay)
            {
                _atEnd = false;

                _currentFrame += _direction;
                _countPlayDelay = 0;

                // A to B
                if (_direction > 0 && _currentFrame > _rangeB)
                {
                    _currentFrame = _rangeA;
                    _atEnd = true;
                }

                // B to A
                if (_direction < 0 && _currentFrame < _rangeA)
                {
                    _currentFrame = _rangeB;
                    _atEnd = true;
                }


            }

            if (_countFrameDelay > _frameDelay)
            {
                ++_countPlayDelay;
                _countFrameDelay = 0;
            }

            ++_countFrameDelay;

            _animation->update();

        }
    }
    void Animate::render()
    {
        if (nullptr != _sequence)
            _animation->render
            (
                _sequence,
                _clip->absX() + _clip->_oX,
                _clip->absY() + _clip->_oY,
                _currentFrame
            );
    }
//}

//{ Draggable

    Clip* Draggable::setDragRect(Rect const& dragRect)
    {
        _rectDrag._x = dragRect._x;
        _rectDrag._y = dragRect._y;
        _rectDrag._w = dragRect._w;
        _rectDrag._h = dragRect._h;

        return _clip;
    }
    Clip* Draggable::setDragRectClip(bool const& isDragRectClip)
    {
        _isDragRectClip = isDragRectClip;

        return _clip;
    }
    Clip* Draggable::setLimitRect(Rect const& limitRect, bool isLimitRect)
    {
        _limitRect = limitRect;
        _isLimitRect = isLimitRect;

        return _clip;
    }
    void Draggable::update()
    {
        if (_isDragRectClip)
        {
            _rectDrag._x = _clip->_rect._x;
            _rectDrag._y = _clip->_rect._y;
            _rectDrag._w = _clip->_rect._w;
            _rectDrag._h = _clip->_rect._h;
        }

        //_mouse.update(mouseX, mouseY, mouseButton);
        //if (Misc::inRect(_mouse->_x, _mouse->_y, _clip->_rect))
        //if (_mouse->_x > _rectDrag._x &&
        //	_mouse->_x < _rectDrag._x + _rectDrag._w &&
        //	_mouse->_y > _rectDrag._y &&
        //	_mouse->_y < _rectDrag._y + _rectDrag._h)

        if (nullptr != _clip->_mouse)
        {
            if (Collision2D::pointRect(Vec2 {_clip->_mouse->_x , _clip->_mouse->_y}, _rectDrag))
                _clip->_isOver = true;
            else
                _clip->_isOver = false;

            if (_clip->_mouse->_isMove && _isDrag && _clip->_isFocus)
            {
                _clip->_x = _clip->_mouse->_x - _dragX;
                _clip->_y = _clip->_mouse->_y - _dragY;
            }

            if (_clip->_mouse->_down)
            {

                if (_clip->_isOver)
                {
                    if (_isDrag) _clip->_isFocus = true;

                    if (!_isDrag && (_clip->_mouse->_button & 1) &&
                        !_clip->_mouse->_drag && !_clip->_mouse->_reSize)
                    {
                        _isDrag = true;
                        _clip->_mouse->_drag = true;
                        //log(0,"< Draggable is On >");
                    }
                }
            }
            else
            {
                if (_isDrag)
                    _isDrag = false;
            }


            if (_isDrag)
            {
                _dragX = _clip->_mouse->_x - _clip->_x;
                _dragY = _clip->_mouse->_y - _clip->_y;

                _clip->updateClipRect();
            }
        }

        if (_isLimitRect)
        {
            if (_clip->_rect._x < _limitRect._x)
            {
                _clip->_x = _limitRect._x + _clip->_oX;
                //_mouse->_x = _rect.x + _dragX;
                _clip->updateClipRect();
                //std::cout << "< Out of LimitRect LEFT >";
            }
            if (_clip->_rect._y < _limitRect._y)
            {
                _clip->_y = _limitRect._y + _clip->_oY;
                //_mouse->_y = _rect.y + _dragY;
                _clip->updateClipRect();
                //std::cout << "< Out of LimitRect TOP >";
            }
            if (_clip->_rect._x > _limitRect._w - _clip->_rect._w)
            {
                _clip->_x = _limitRect._w - _clip->_rect._w + _clip->_oX;
                //_mouse->_x = _rect.x + _dragX;
                _clip->updateClipRect();
                //std::cout << "< Out of LimitRect RIGHT >";
            }
            if (_clip->_rect._y > _limitRect._h - _clip->_rect._h)
            {
                _clip->_y = _limitRect._h - _clip->_rect._h + _clip->_oY;
                //_mouse->_y = _rect.y + _dragY;
                _clip->updateClipRect();
                //std::cout << "< Out of LimitRect BOTTOM >";
            }
        }

    }
    void Draggable::render()
    {

        if (nullptr != _clip)
        {
            // Draw the rectDrag Zone !
            al_draw_filled_rectangle(.5+_rectDrag._x,
                                     .5+_rectDrag._y,
                                     .5+_rectDrag._x + _rectDrag._w,
                                     .5+_rectDrag._y + _rectDrag._h,
                                     al_map_rgba(25, 50, 80, 250));
            if (!_clip->_isFocus)
            {
                al_draw_rectangle(.5+_clip->_rect._x,
                                  .5+_clip->_rect._y,
                                  .5+_clip->_rect._x + _clip->_rect._w,
                                  .5+_clip->_rect._y + _clip->_rect._h,
                                  al_map_rgba(155, 20, 50, 55), 0);


            }	else
            {
                al_draw_filled_rectangle(.5+_rectDrag._x,
                                         .5+_rectDrag._y,
                                         .5+_rectDrag._x + _rectDrag._w,
                                         .5+_rectDrag._y + _rectDrag._h,
                                         al_map_rgba(55, 100, 150, 250));
            }


            if (_clip->_isOver)
            {
                if (_isDrag)
                {
                    al_draw_rectangle(.5+_clip->_rect._x,
                                      .5+_clip->_rect._y,
                                      .5+_clip->_rect._x + _clip->_rect._w,
                                      .5+_clip->_rect._y + _clip->_rect._h,
                                      al_map_rgb(255, 120, 0), 0);
                }
                else
                {
                    al_draw_rectangle(.5+_clip->_rect._x,
                                      .5+_clip->_rect._y,
                                      .5+_clip->_rect._x + _clip->_rect._w,
                                      .5+_clip->_rect._y + _clip->_rect._h,
                                      al_map_rgb(55, 250, 180), 0);
                }
            }
        }
    }
//}

//{ Camera

    Clip* Camera::setMainClip(Clip* mainClip)
    {
        _mainClip = mainClip;
        return _clip;
    }
    Clip* Camera::setRectView(Rect const& rectView)
    {
        _rectView = rectView;
        return _clip;
    }
    Clip* Camera::setRectLimitZone(Rect const& rectLimitZone)
    {
        _isLimitZone = true;
        _rectLimitZone = rectLimitZone;
        return _clip;
    }
    Clip* Camera::setLimitZone(bool isLimitZone)
    {
        _isLimitZone = isLimitZone;
        return _clip;
    }
    void Camera::setPosition(VAR x, VAR y)
    {
        _rectView._x = x;
        _rectView._y = y;

        limitViewZone();
    }
    void Camera::limitViewZone()
    {
        if (_isLimitZone)
        {
            if (_rectView._x < _rectLimitZone._x)
                _rectView._x = _rectLimitZone._x;

            if (_rectView._y < _rectLimitZone._y)
                _rectView._y = _rectLimitZone._y;

            if (_rectView._x > _rectLimitZone._w - _rectView._w)
                _rectView._x = _rectLimitZone._w - _rectView._w;

            if (_rectView._y > _rectLimitZone._h - _rectView._h)
                _rectView._y = _rectLimitZone._h - _rectView._h;
        }

    }
    void Camera::moveX(VAR vx)
    {
        _rectView._x += vx;
        limitViewZone();
    }
    void Camera::moveY(VAR vy)
    {
        _rectView._y += vy;
        limitViewZone();
    }
    void Camera::update()
    {
        if (nullptr != _clip->_parent)
            for (unsigned i = 0; i < _clip->_parent->vecSize(); ++i)
            {
                _clip->_parent->index(i)->_x = -(_rectView._x * _clip->_parent->index(i)->_cameraMoveFactor);
                _clip->_parent->index(i)->_y = -(_rectView._y * _clip->_parent->index(i)->_cameraMoveFactor);
            }
    }
    void Camera::render()
    {

    }
//}

//{ TileMap2D

    void TileMap2D::setTile(int x, int y, Tile* tile)
    {
        _map2D->put(x,y,tile);
    }
    Clip* TileMap2D::setup(Rect const& rectView, unsigned mapW, unsigned mapH, VAR tileW, VAR tileH, VAR x, VAR y)
    {
        //_type = Component::type("TILEMAP2D", true);

        _tileW = tileW;
        _tileH = tileH;

        _rect._x = x;
        _rect._y = y;
        _rect._w = mapW*tileW;
        _rect._h = mapH*tileH;

        _rectView = rectView;

        _map2D = new Map2D<Tile>(mapW, mapH, new Tile());

        return _clip;
    }

    Clip* TileMap2D::loadTiledJSON
    (
        std::string const& jsonFileName,
        int idLayer,
        int idTileSet,
        std::string const& tileSetName,
        Asset::Manager* asset,
        std::string const& path,
        std::map<int,std::string> mapTiledProperty
    )
    {

        if (nullptr == asset)
        {
            mlog("- Error loadTiledJson : asset == nullptr",1);
            return _clip;
        }


        _jsonFileName = jsonFileName;
        _idLayer = idLayer;
        _idTileSet = idTileSet;
        _mapTiledProperty = mapTiledProperty;

        Json mapTiledJSON = File::loadJson(_jsonFileName);

        std::string tileSetFileName = mapTiledJSON["tilesets"][idTileSet]["image"];

        asset->add(new Asset::Bitmap(tileSetName, (path+tileSetFileName).c_str() ));

        setAtlas(asset->GET_BITMAP(tileSetName));

        int tw      = mapTiledJSON["tilesets"][idTileSet]["tilewidth"];
        int th      = mapTiledJSON["tilesets"][idTileSet]["tileheight"];
        unsigned mw = mapTiledJSON["layers"][idLayer]["width"];
        unsigned mh = mapTiledJSON["layers"][idLayer]["height"];
        int nbCol   = mapTiledJSON["tilesets"][idTileSet]["columns"];


        setTileSize(tw,th);
        setMapSize(mw,mh);


        for (unsigned y=0; y<mh; ++y)
        {
            for (unsigned x=0; x <mw; ++x)
            {
                int id = mapTiledJSON["layers"][idLayer]["data"][x + y*mw];
                int firstgid = mapTiledJSON["tilesets"][idTileSet]["firstgid"];
                id = id - firstgid;

                if (id != 0)
                {
                    int my = round(id/nbCol);
                    int mx = id - my * nbCol;

                    VAR tx = mx * tw;
                    VAR ty = my * th;

                    Tile* tile = new Tile(0,id, Rect{tx,ty,(VAR)tw,(VAR)th});

                    if (nullptr != mapTiledJSON["tilesets"][idTileSet]["tileproperties"][std::to_string(id)]["isCollidable"])
                    {
                        int isCollidable = mapTiledJSON["tilesets"][idTileSet]["tileproperties"][std::to_string(id)]["isCollidable"];
                        tile->_isCollidable = isCollidable;
                    }

                    // Iterate all tiles property !
                    auto it = mapTiledProperty.begin();
                    while(it != mapTiledProperty.end())
                    {
                        if (nullptr != mapTiledJSON["tilesets"][idTileSet]["tileproperties"][std::to_string(id)][mapTiledProperty[it->first]])
                            tile->_mapProperty[it->first] = mapTiledJSON["tilesets"][idTileSet]["tileproperties"][std::to_string(id)][mapTiledProperty[it->first]];
                        ++it;
                    }

                    setTile(x, y, tile);
                }
            }
        }

        _mapLoaded = true;

        return _clip;
    }
    Clip* TileMap2D::reLoadTileProperty()
    {
        if (_mapLoaded)
        {
            Json mapTiledJSON = File::loadJson(_jsonFileName);

            unsigned mw = mapTiledJSON["layers"][_idLayer]["width"];
            unsigned mh = mapTiledJSON["layers"][_idLayer]["height"];

            for (unsigned y=0; y<mh; ++y)
            {
                for (unsigned x=0; x <mw; ++x)
                {
                    int id = mapTiledJSON["layers"][_idLayer]["data"][x + y*mw];
                    int firstgid = mapTiledJSON["tilesets"][_idTileSet]["firstgid"];
                    id = id - firstgid;

                    if (id != 0)
                    {
                        Tile* tile = getTile(x,y);

                        if (nullptr != mapTiledJSON["tilesets"][_idTileSet]["tileproperties"][std::to_string(id)]["isCollidable"])
                        {
                            int isCollidable = mapTiledJSON["tilesets"][_idTileSet]["tileproperties"][std::to_string(id)]["isCollidable"];
                            tile->_isCollidable = isCollidable;
                        }

                        // Iterate all tiles property !
                        auto it = _mapTiledProperty.begin();
                        while(it != _mapTiledProperty.end())
                        {
                            if (nullptr != mapTiledJSON["tilesets"][_idTileSet]["tileproperties"][std::to_string(id)][_mapTiledProperty[it->first]])
                                tile->_mapProperty[it->first] = mapTiledJSON["tilesets"][_idTileSet]["tileproperties"][std::to_string(id)][_mapTiledProperty[it->first]];
                            ++it;
                        }
                    }
                }
            }

        }
        return _clip;
    }
    Clip* TileMap2D::setRectView(Rect const& rectView)
    {
        _rectView = rectView;
        return _clip;
    }
    Clip* TileMap2D::setMapSize(unsigned mapW, unsigned mapH)
    {
        _map2D->resizeVecObject2D(mapW, mapH);
        return _clip;
    }
    Clip* TileMap2D::setTileSize(VAR tileW, VAR tileH)
    {
        _tileW = tileW;
        _tileH = tileH;
        return _clip;
    }
    Clip* TileMap2D::setAtlas(ALLEGRO_BITMAP* atlas)
    {
        _atlas = atlas;
        return _clip;
    }
    Clip* TileMap2D::setPosition(VAR x, VAR y)
    {
        _rect._x = x;
        _rect._y = y;
        return _clip;
    }
    Tile* TileMap2D::getTile(int x, int y)
    {
        return _map2D->get(x,y);
    }
    int TileMap2D::getTileType(int x, int y)
    {
        Tile* tile = _map2D->get(x, y);
        if (nullptr != tile)
            return tile->_type;

        return 0;
    }
    int TileMap2D::getTileProperty(int x, int y, int index)
    {
        Tile* tile = _map2D->get(x, y);
        if (nullptr != tile)
            return tile->_mapProperty[index];

        return 0;
    }
    void TileMap2D::setTileProperty(int x, int y, int index, int value)
    {
        Tile* tile = _map2D->get(x, y);
        if (nullptr != tile)
            tile->_mapProperty[index] = value;
    }

    int TileMap2D::getTileCollidable(int x, int y)
    {
        Tile* tile = _map2D->get(x, y);
        if (nullptr != tile)
            return tile->_isCollidable;

        return 0;
    }
    Rect TileMap2D::getTileRect(int x, int y, VAR extend)
    {
        Rect rect=
        {
            _tileW*x-extend,
            _tileH*y-extend,
            _tileW+extend*2,
            _tileH+extend*2
        };

        return rect;
    }
    void TileMap2D::showGrid(Draw::Color const& color)
    {
        if (Clip::_showClipInfo)
            Draw::grid
            (
                _rect._x,
                _rect._y,
                _rect._w,
                _rect._h,
                _tileW,
                _tileH,
                color
            );
    }
    void TileMap2D::showInfo(Draw::Font* font, Draw::Color const& color)
    {
        if (Clip::_showClipInfo)
            for (unsigned x = 0; x < _map2D->_mapW; ++x)
            {
                if (x*_tileW+_rect._x < _rectView._x - _tileW || x*_tileW+_rect._x > _rectView._w)
                    continue;

                for (unsigned y = 0; y < _map2D->_mapH; ++y)
                {

                    if(y*_tileH+_rect._y < _rectView._y - _tileH || y*_tileH+_rect._y > _rectView._h)
                        continue;

                    Tile* tile = _map2D->get(x,y);

                    if (nullptr != tile)
                    {
                        //Draw::rect(Rect{x*_tileW+_rect._x, y*_tileH+_rect._y, _tileW, _tileH}, al_map_rgb(150,106,150),0);

                        if (tile->_rect._w < 1 || tile->_rect._h < 1)
                            continue;

                        //Draw::rect(Rect{x*_tileW+_rect._x, y*_tileH+_rect._y, _tileW, _tileH}, al_map_rgb(150,106,150),0);

                        al_draw_textf
                        (
                            font, color,
                            x*_tileW + 4 + _rect._x,
                            y*_tileH + 2 + _rect._y,
                            0,
                            "%i", tile->_type
                        );
                        al_draw_textf
                        (
                            font, color,
                            x*_tileW + 4 + _rect._x,
                            y*_tileH + 12 + _rect._y,
                            0,
                            "%i %i", x,y
                        );
                    }
                }
            }
    }
    void TileMap2D::update()
    {
        _rect._x = _clip->absX();
        _rect._y = _clip->absY();
    }
    void TileMap2D::render()
    {
        Draw::bitmapBegin(_drawList);

        for (unsigned x = 0; x < _map2D->_mapW; ++x)
        {
            if (x*_tileW+_rect._x < _rectView._x - _tileW || x*_tileW+_rect._x > _rectView._w)
                continue;

            for (unsigned y = 0; y < _map2D->_mapH; ++y)
            {

                if(y*_tileH+_rect._y < _rectView._y - _tileH || y*_tileH+_rect._y > _rectView._h)
                    continue;

                Tile* tile = _map2D->get(x,y);

                if (nullptr != tile)
                {


                    if (tile->_rect._w < 1 || tile->_rect._h < 1)
                        continue;

                    Draw::bitmapBatch
                    (
                        _drawList,
                        tile->_rect,
                        Rect{std::floor(x*_tileW+_rect._x), std::floor(y*_tileH+_rect._y), _tileW, _tileH}
                    );

                }

            }
        }

        Draw::bitmapEnd(_drawList, _atlas);
    }
//}

//{ Transition

    bool Transition::onPos(int pos)
    {
        return _currentPos == pos;
    }
    Clip* Transition::setPos(int pos)
    {
        _currentPos = pos;
        return _clip;
    }
//}

//{ Viewport

    Viewport::~Viewport()
    {
        if (!_view.empty())
        {
            auto it = _view.begin();
            while (it != _view.end())
            {
                if (nullptr != it->second)
                {
                    delete it->second;
                    it->second = nullptr;
                }
                ++it;
            }
            _view.clear();
        }
    }
    Viewport::View* Viewport::getView(int viewId)
    {
        if ( _view.find(viewId) == _view.end())
            return nullptr;
        else
            return _view[viewId];
    }

    Rect Viewport::getRect(int viewId)
    {
        return _view[viewId]->_rect;
    }

    Clip* Viewport::setViewport(std::shared_ptr<Window> window, int viewId, Rect const& rect)
    {
        _view[viewId] = new View
        {
            al_create_bitmap(window->screenW(), window->screenH()),
            rect
        };
        return _clip;
    }
    Clip* Viewport::updateViewport(int viewId, Rect const& rect)
    {
        _view[viewId]->_rect = rect;
        return _clip;
    }
    Clip* Viewport::beginRenderView(std::shared_ptr<Window> window, int viewId)
    {
        al_set_target_bitmap(_view[viewId]->_renderTarget2D);
        return _clip;
    }
    Clip* Viewport::endRenderView(std::shared_ptr<Window> window, int viewId)
    {
        al_set_target_bitmap(window->buffer());
        al_draw_bitmap_region
        (
            _view[viewId]->_renderTarget2D,
            0,0,
            _view[viewId]->_rect._w,
            _view[viewId]->_rect._h,
            _view[viewId]->_rect._x,
            _view[viewId]->_rect._y,
            0
        );

        return _clip;
    }

//}

}

