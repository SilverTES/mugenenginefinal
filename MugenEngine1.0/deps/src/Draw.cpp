#include "Draw.h"

//------------------------------------------------------------------------------ Primitives

void Draw::lineBegin(List& drawList)
{
    drawList.clear();
}

void Draw::lineEnd(List& drawList)
{
    if (!drawList.empty())
        al_draw_prim(&drawList[0], nullptr, nullptr, 0, drawList.size(), ALLEGRO_PRIM_LINE_LIST);
}

void Draw::line(VAR x1, VAR y1, VAR x2, VAR y2, Color const& color, VAR thickness)
{
    al_draw_line(.5+x1,.5+y1,.5+x2,.5+y2,color,thickness);
}

void Draw::lineAA(VAR x1, VAR y1, VAR x2, VAR y2, RGBA const& color, VAR thickness)
{
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);

    glLineWidth(thickness);
    glBegin(GL_LINES);
        //glColor4ub(255,0,0,180);
        //glColor4f(1.0f,0.0f,1.0f,0.5f);
        //glColor4ub(color.r,color.g,color.b,color.a);
        glColor4ub(color._r,color._g,color._b,color._a);
        glVertex2f(x1, y1);
        glVertex2f(x2, y2);
    glEnd();

    glLineWidth(1);
    glDisable(GL_LINE_SMOOTH);
}

void Draw::lineBatch(List& drawList, VAR x1, VAR y1, VAR x2, VAR y2, Color const& color)
{
    drawList.push_back
    (
        ALLEGRO_VERTEX
        {
            0.5f+x1,0.5f+y1,0.0f,
            0,0,
            color
        }
    );
    drawList.push_back
    (
        ALLEGRO_VERTEX
        {
            0.5f+x2,0.5f+y2,0.0f,
            0,0,
            color
        }
    );

}

void Draw::sight(List& drawList, VAR x, VAR y, int screenW, int screenH, Color const& color)
{
    lineBatch(drawList,0,y,screenW,y,color);
    lineBatch(drawList,x,0,x,screenH,color);
}

void Draw::grid(VAR x, VAR y, VAR w, VAR h, int sizeX, int sizeY, Color const& color)
{
    List drawList;
    lineBegin(drawList);
    for (int i(0); i<std::floor(w/sizeX)+1; i++)
    {
        lineBatch(drawList,std::floor(i*sizeX+x), std::floor(y), std::floor(i*sizeX+x), std::floor(h+y), color);
    }

    for (int i(0); i<std::floor(h/sizeY)+1; i++)
    {
        lineBatch(drawList,std::floor(x), std::floor(i*sizeY+y), std::floor(w+x), std::floor(i*sizeY+y), color);
    }
    lineEnd(drawList);
}
void Draw::grid(Rect const& rect, int sizeX, int sizeY, Color const& color)
{
    grid(rect._x, rect._y, rect._w, rect._h, sizeX, sizeY, color);
}
void Draw::gridBatch(List& drawList, VAR x, VAR y, VAR w, VAR h, int sizeX, int sizeY, Color const& color)
{
    for (int i(0); i<std::floor(w/sizeX)+1; i++)
    {
        //lineBatch(drawList,i*sizeX+x, y, i*sizeX+x, h+y, color);
        lineBatch(drawList,std::floor(i*sizeX+x), std::floor(y), std::floor(i*sizeX+x), std::floor(h+y), color);
    }

    for (int i(0); i<std::floor(h/sizeY)+1; i++)
    {
        //lineBatch(drawList,x, i*sizeY+y, w+x, i*sizeY+y, color);
        lineBatch(drawList,std::floor(x), std::floor(i*sizeY+y), std::floor(w+x), std::floor(i*sizeY+y), color);
    }
}
void Draw::gridBatch(List& drawList, Rect const& rect, int sizeX, int sizeY, Color const& color)
{
    gridBatch(drawList, rect._x, rect._y, rect._w, rect._h, sizeX, sizeY, color);
}


void Draw::losange(List& drawList, VAR x, VAR y, VAR w, VAR h, Color const& color)
{
    lineBatch(drawList,x, y+h/2, x+w/2, y, color);
    lineBatch(drawList,x+w, y+h/2, x+w/2, y, color);
    lineBatch(drawList,x, y+h/2, x+w/2, y+h, color);
    lineBatch(drawList,x+w, y+h/2, x+w/2, y+h, color);
}


void Draw::triangle(Triangle triangle, Color color, VAR thickness)
{
    al_draw_triangle
    (
        triangle._x1, triangle._y1,
        triangle._x2, triangle._y2,
        triangle._x3, triangle._y3,
        color,
        thickness
    );
}
void Draw::triangleFill(Triangle triangle, Color color)
{
    al_draw_filled_triangle
    (
        triangle._x1, triangle._y1,
        triangle._x2, triangle._y2,
        triangle._x3, triangle._y3,
        color
    );
}
void Draw::rect(Rect rect, Color color, VAR thickness)
{
    al_draw_rectangle(.5+rect._x,.5+rect._y,rect._x+rect._w-.5,rect._y+rect._h-.5,color,thickness);
}
void Draw::rectFill(Rect rect, Color color)
{
    al_draw_filled_rectangle(rect._x,rect._y,rect._x+rect._w,rect._y+rect._h,color);
}
void Draw::roundRect(Rect rect, VAR rx, VAR ry, Color color, VAR thickness)
{
    al_draw_rounded_rectangle(.5+rect._x,.5+rect._y,rect._x+rect._w-.5,rect._y+rect._h-.5,rx,ry,color,thickness);
}

void Draw::roundRectFill(Rect rect, VAR rx, VAR ry, Color color)
{
    al_draw_filled_rounded_rectangle(rect._x,rect._y,rect._x+rect._w,rect._y+rect._h,rx,ry,color);
}
void Draw::circle(Circle circle, Color color, VAR thickness)
{
    al_draw_circle(.5+circle._x,.5+circle._y,circle._r,color,thickness);
}
void Draw::circleFill(Circle circle, Color color)
{
    al_draw_filled_circle(.5+circle._x,.5+circle._y,circle._r,color);
}

//------------------------------------------------------------------------------ Bitmap

void Draw::bitmapBegin(List& drawList)
{
    drawList.clear();
}

void Draw::bitmapEnd(List& drawList, Bitmap* atlas)
{
    if (!drawList.empty() && (atlas != nullptr))
        al_draw_prim(&drawList[0], nullptr, atlas, 0, drawList.size(), ALLEGRO_PRIM_TRIANGLE_LIST);
}

void Draw::bitmapBatch(List& drawList, Rect rectSrc, Rect rectDest, Color col)
{
    VAR sx = rectSrc._x;
    VAR sy = rectSrc._y;
    VAR sw = rectSrc._w;
    VAR sh = rectSrc._h;

    VAR dx = rectDest._x;
    VAR dy = rectDest._y;
    VAR dw = rectDest._w;
    VAR dh = rectDest._h;

    VAR z = 0;

    /*
        A         B
          o-----o
          |    /|
          |  /  |
          |/    |
          o-----o
        C         D
    */

    drawList.push_back // A
    (
        ALLEGRO_VERTEX
        {
            dx    , dy    , z,
            sx    , sy    , col
        }
    );
    drawList.push_back // B
    (
        ALLEGRO_VERTEX
        {
            dx + dw, dy    , z,
            sx + sw, sy    , col
        }
    );
    drawList.push_back // C
    (
        ALLEGRO_VERTEX
        {
            dx    , dy + dh, z,
            sx    , sy + sh, col
        }
    );
    drawList.push_back // C
    (
        ALLEGRO_VERTEX
        {
            dx    , dy + dh, z,
            sx    , sy + sh, col
        }
    );
    drawList.push_back // D
    (
        ALLEGRO_VERTEX
        {
            dx + dw, dy + dh, z,
            sx + sw, sy + sh, col
        }
    );
    drawList.push_back // B
    (
        ALLEGRO_VERTEX
        {
            dx + dw, dy    , z,
            sx + sw, sy    , col
        }
    );
}


void Draw::mosaic(Rect rectView, VAR x, VAR y, int repX, int repY, ALLEGRO_BITMAP* bitmap, ALLEGRO_COLOR const& color)
{
    VAR tileW = al_get_bitmap_width(bitmap);
    VAR tileH = al_get_bitmap_height(bitmap);

    List drawList;
    bitmapBegin(drawList);

    for (int i = 0; i < repX; ++i)
    {
        if (x+i*tileW < rectView._x - tileW || x+i*tileW > rectView._w)
            continue;

        for (int j = 0; j < repY; ++j)
        {
            if(y+j*tileH < rectView._y - tileH || y+j*tileH > rectView._h)
                continue;
            bitmapBatch(drawList,Rect{0,0,tileW,tileH}, Rect{std::floor(x+i*tileW),std::floor(y+j*tileH),tileW,tileH}, color);
        }
    }
    bitmapEnd(drawList,bitmap);
}

void Draw::mosaic(Rect rectView, VAR x, VAR y, int repX, int repY, ALLEGRO_BITMAP* bitmap, VAR tileX, VAR tileY, VAR tileW, VAR tileH, ALLEGRO_COLOR const& color)
{
    List drawList;
    bitmapBegin(drawList);

    for (int i = 0; i < repX; ++i)
    {
        if (x+i*tileW < rectView._x - tileW || x+i*tileW > rectView._w)
            continue;

        for (int j = 0; j < repY; ++j)
        {
            if(y+j*tileH < rectView._y - tileH || y+j*tileH > rectView._h)
                continue;
            bitmapBatch(drawList,Rect{tileX,tileY,tileW,tileH}, Rect{std::floor(x+i*tileW),std::floor(y+j*tileH),tileW,tileH}, color);
        }
    }
    bitmapEnd(drawList,bitmap);
}

void Draw::mosaicBatch(List& drawList, Rect rectView, VAR x, VAR y, int repX, int repY, ALLEGRO_BITMAP* bitmap, ALLEGRO_COLOR const& color)
{
    VAR tileW = al_get_bitmap_width(bitmap);
    VAR tileH = al_get_bitmap_height(bitmap);


    for (int i = 0; i < repX; ++i)
    {
        if (x+i*tileW < rectView._x - tileW || x+i*tileW > rectView._w)
            continue;

        for (int j = 0; j < repY; ++j)
        {
            if(y+j*tileH < rectView._y - tileH || y+j*tileH > rectView._h)
                continue;

            bitmapBatch(drawList,Rect{0,0,tileW,tileH}, Rect{std::floor(x+i*tileW),std::floor(y+j*tileH),tileW,tileH}, color);
        }
    }
}

//------------------------------------------------------------------------------ Shader

ALLEGRO_SHADER* Draw::createShader(std::string const& fileNameVert, std::string const& fileNameFrag )
{
    ALLEGRO_SHADER* shader = al_create_shader(ALLEGRO_SHADER_GLSL);
//    if(!al_attach_shader_source(shader,
//                                ALLEGRO_VERTEX_SHADER,
//                                al_get_default_shader_source(ALLEGRO_SHADER_GLSL, ALLEGRO_VERTEX_SHADER)))
    if(!al_attach_shader_source_file(shader, ALLEGRO_VERTEX_SHADER, fileNameVert.c_str()))
    {
        printf("%s\n", al_get_shader_log(shader));
        return NULL;
    }
    if(!al_attach_shader_source_file(shader, ALLEGRO_PIXEL_SHADER, fileNameFrag.c_str()))
    {
        printf("%s\n", al_get_shader_log(shader));
        return NULL;
    }
    if(!al_build_shader(shader))
    {
        printf("%s\n", al_get_shader_log(shader));
        return NULL;
    }
    //std::cout << " >>> "<< al_get_shader_log(shader) << " \n";

    return shader;
}
