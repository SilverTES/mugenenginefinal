//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#include "Framerate.h"

int Framerate::_nbFrame = 0;
int Framerate::_nbFps = 0;
long double Framerate::_gameTime = 0;
long double Framerate::_oldTime = 0;

Framerate::Framerate(ALLEGRO_EVENT_QUEUE* eventQueue, double framerate)
{
    _prevTime = 0;
    _restTime = 0;
    _currentTime = 0;

    _framerate = framerate;
    _framerateTimer = al_create_timer(1.0f/_framerate);

    al_register_event_source (eventQueue,al_get_timer_event_source(_framerateTimer));
    al_start_timer(_framerateTimer);
}

Framerate::~Framerate()
{
    if (_framerateTimer) al_destroy_timer(_framerateTimer);
}

//------------------------------------------------------------------------------ Framerate
void Framerate::pollFramerate()
{
    ++_nbFrame;

    _gameTime = al_get_time();
    if (_gameTime - _oldTime > 1.000f)
    {
        _nbFps = std::round(_nbFrame / (_gameTime - _oldTime)) ;
        _nbFrame = 0;
        _oldTime = _gameTime;
    }
}
int Framerate::getFramerate()
{
    return _nbFps;
}

