#include "Collision2DGrid.h"
#include "Draw.h"

namespace Collision2D
{
    GridSystem::GridSystem(int gridW, int gridH, int cellSize)
    {
        _cellSize = cellSize;

        _gridW = gridW;
        _gridH = gridH;

        _vec2dCell.resize(_gridW);
        for (int x = 0; x < _gridW; ++x)
        {
            _vec2dCell[x].resize(_gridH);
            for (int y = 0; y < _gridH; ++y)
            {
                _vec2dCell[x][y] = new Collide::Cell();
            }
        }
    }

    GridSystem::~GridSystem()
    {
        clearAll();
    }

    Collide::Cell *GridSystem::cell(unsigned x, unsigned y)
    {
        return _vec2dCell[x][y];
    }

    void GridSystem::clearAll()
    {
        for (int x = 0; x < _gridW; ++x)
        {
            for (int y = 0; y < _gridH; ++y)
            {
                if (cell(x,y) != nullptr)
                {
                    if (!cell(x, y)->_vecCollideZoneInCell.empty())
                    {
                        for (auto & it: cell(x, y)->_vecCollideZoneInCell)
                            if (it != nullptr)
                            {
                                delete it;
                                it = nullptr;
                            }

                        cell(x, y)->_vecCollideZoneInCell.clear();

                    }
                }
            }
        }
    }

    void GridSystem::insert(unsigned index, Rect const& rect, Clip* clip)
    {
        int x = rect._x - _originX - _cellSize;
        int y = rect._y - _originY - _cellSize;

        int left = std::max(0, x / _cellSize);
        int top = std::max(0, y / _cellSize);
        int right = std::min((float)_gridW-1,(x + rect._w - 1) / _cellSize);
        int bottom = std::min((float)_gridH-1,(y + rect._h - 1) / _cellSize);

        for (int x = left; x <= right; ++x)
        {
            for (int y = top; y <= bottom; ++y)
            {
                cell(x, y)->_vecCollideZoneInCell.push_back(new Collide::Zone{ index, rect, clip });
            }
        }

    }

    void GridSystem::add(Collide::Zone* collideZone)
    {
        int x = collideZone->_rect._x - _originX - _cellSize;
        int y = collideZone->_rect._y - _originY - _cellSize;

        int left = std::max(0, x / _cellSize);
        int top = std::max(0, y / _cellSize);
        int right = std::min((float)_gridW-1,(x + collideZone->_rect._w - 1) / _cellSize);
        int bottom = std::min((float)_gridH-1,(y + collideZone->_rect._h - 1) / _cellSize);

        for (int x = left; x <= right; ++x)
        {
            for (int y = top; y <= bottom; ++y)
            {
                cell(x, y)->_vecCollideZoneInCell.push_back
                (
                    new Collide::Zone
                    {
                        collideZone->_index,
                        collideZone->_rect,
                        collideZone->_clip
                    }
                );
            }
        }
    }



    std::vector<Collide::Zone*> GridSystem::findNear(std::vector<Collide::Zone*> &_vecZoneTemp, Rect rect)
    {
        int x = rect._x - _originX - _cellSize;
        int y = rect._y - _originY - _cellSize;

        int left = std::max(0, x / _cellSize);
        int top = std::max(0, y / _cellSize);
        int right = std::min((float)_gridW-1,(x + rect._w - 1) / _cellSize);
        int bottom = std::min((float)_gridH-1,(y + rect._h - 1) / _cellSize);

        for (int x = left; x <= right; ++x)
        {
            for (int y = top; y <= bottom; ++y)
            {
                for (unsigned i = 0; i < cell(x, y)->_vecCollideZoneInCell.size(); ++i)
                {
                    Collide::Zone *collideZone = cell(x, y)->_vecCollideZoneInCell[i];

                    //if (collideZone->_index != index)
                        _vecZoneTemp.push_back(collideZone);
                }

            }
        }
        return _vecZoneTemp;
    }

    std::vector<Collide::Zone*> GridSystem::findNear
    (
        std::vector<Collide::Zone*>& _vecZoneTemp,
        Collide::Zone* zoneTest
    )
    {
        int x = zoneTest->_rect._x - _originX - _cellSize;
        int y = zoneTest->_rect._y - _originY - _cellSize;

        int left = std::max(0, x / _cellSize);
        int top = std::max(0, y / _cellSize);
        int right = std::min((float)_gridW-1,(x + zoneTest->_rect._w - 1) / _cellSize);
        int bottom = std::min((float)_gridH-1,(y + zoneTest->_rect._h - 1) / _cellSize);

        for (int x = left; x <= right; ++x)
        {
            for (int y = top; y <= bottom; ++y)
            {
                for (unsigned i = 0; i < cell(x, y)->_vecCollideZoneInCell.size(); ++i)
                {
                    _vecZoneTemp.push_back(cell(x, y)->_vecCollideZoneInCell[i]);
                }

            }
        }

        return _vecZoneTemp;
    }







    Collide::Zone* GridSystem::getNearest(Rect rect)
    {
    //            CollideZone *nearest = nullptr;
    //            int distance = std::numeric_limits<int>::max();
        return nullptr;
    }

    void GridSystem::setPosition(int x, int y)
    {
        _originX = x;
        _originY = y;
    }

    void GridSystem::update()
    {

    }

    void GridSystem::render()
    {
        for (int x = 0; x <= _gridW; ++x)
        {
            for (int y = 0; y <= _gridH; ++y)
            {

                VAR px = x*_cellSize + _originX;
                VAR py = y*_cellSize + _originY;


                al_draw_rectangle(
                    .5+px, .5+py,
                    .5+px + _cellSize, .5+py + _cellSize,
                    al_map_rgba(55, 55, 55, 55), 0
                );

                //al_draw_textf(
                //	font,
                //	al_map_rgb(250,250,0),
                //	px + 2, py + 2, 0,
                //	"%i,%i = %i",
                //	x, y, cell(x, y)->_vecCollideZoneInCell.size()
                //);

                if (!(cell(x, y)->_vecCollideZoneInCell.empty()))
                {
                    //log("not empty !\n");

                    //for (unsigned i = 0; i < cell(x, y)->_vecCollideZoneInCell.size(); ++i)
                    //{
                    //	al_draw_textf(font, al_map_rgb(25, 205, 255),
                    //				  px + 2, py+12 + (i * 12)  , 0,
                    //				  "vec[%i]=%i", i, cell(x, y)->_vecCollideZoneInCell[i]->_index
                    //	);
                    //}
                }
            }
        }
    }


}
