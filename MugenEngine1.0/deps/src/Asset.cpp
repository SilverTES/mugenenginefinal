#include "Asset.h"

namespace Asset
{
    Bitmap::Bitmap(std::string name, const char* fileName)
    {
        _name = name;
        _fileName = fileName;
        _data = al_load_bitmap(fileName);
    }
    Bitmap::~Bitmap()
    {
        al_destroy_bitmap(_data);
    }

    Font::Font(std::string name, const char* fileName, int fontSize, int flags)
    {
        _name = name;
        _fileName = fileName;
        _data = al_load_font(fileName, fontSize, flags);
    }
    Font::~Font()
    {
        al_destroy_font(_data);
    }

    Sample::Sample(std::string name, const char* fileName)
    {
        _name = name;
        _fileName = fileName;
        _data = al_load_sample(fileName);
    }
    Sample::~Sample()
    {
        al_destroy_sample(_data);
    }

    Manager::Manager(std::string const& name)
    {
        _name = name;
    }

    Manager::~Manager()
    {
        if (!_mapAsset.empty())
        {
            auto it = _mapAsset.begin();

            while (it != _mapAsset.end())
            {
                if (it->second != nullptr)
                {

                    delete it->second;
                    it->second = nullptr;
                    //it = _mapAsset.erase(it);
                }

                ++it;
            }

            _mapAsset.clear();
        }

    }

    Manager* Manager::add(Data* content)
    {
        if (content == nullptr)
            return nullptr;

        _mapAsset.insert(std::pair<std::string, Data*>(content->_name, content) );
        return this;
    }

    bool Manager::del(std::string name)
    {
        auto it = _mapAsset.find(name);

        if (it == _mapAsset.end())
        {
            std::cout << "Not found : "<< name << "\n";
            return false;
        }

        if (it->second != nullptr)
        {
            delete it->second;
            it->second = nullptr;
        }

        _mapAsset.erase(it);
        //std::cout << "item found : "<< name << "\n";
        return true;
    }

    void Manager::showAll()
    {
        auto it = _mapAsset.begin();

        while (it != _mapAsset.end())
        {
            if ((*it).second != nullptr)
                #ifdef SHOW_LOG
                std::cout << "[ "<< it->second->_id << " = " << it->second->_name << " ]\n";
                #endif // SHOW_LOG

            ++it;
        }
    }

    Manager* loadJSON(std::string const& fileName)
    {
        Json asset = File::loadJson(fileName);
        Manager* assetManager = nullptr;

//        std::cout << asset["name"] << "\n";
//        std::cout << asset["asset"].dump(2) << "\n";

        std::string name;

        if (nullptr != asset["name"])
        {
            name = asset["name"];
            assetManager = new Manager(name);
        }
        else
        {
            mlog("- Error load Json Asset : need name !\n");
            return nullptr;
        }

        if (nullptr != asset["asset"])
        {
            for (unsigned i = 0; i < asset["asset"].size(); ++i)
            {
                Json::iterator it = asset["asset"][i].begin();

                std::string name = "";
                std::string file = "";

                if (it.key() == "font" ||
                    it.key() == "bitmap" ||
                    it.key() == "sample")
                {
                    name = asset["asset"][i][it.key()][0];
                    file = asset["asset"][i][it.key()][1];
                }

                if (it.key() == "font")
                {
                    assetManager->add( new Asset::Font(name.c_str(), file.c_str(), asset["asset"][i][it.key()][2], asset["asset"][i][it.key()][3]) );
                }
                else if (it.key() == "bitmap")
                {
                    assetManager->add( new Asset::Bitmap(name.c_str(), file.c_str()) );
                }
                else if (it.key() == "sample")
                {
                    assetManager->add( new Asset::Sample(name.c_str(), file.c_str()) );
                }

                //std::cout << "Asset : " << name << " , " << file << "\n" ;

            }

            return assetManager;

        }
        else
        {
            mlog("- Error load Json Asset : need asset collection !\n");
            return nullptr;
        }

    }

}
