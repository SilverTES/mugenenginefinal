#include "Input.h"

std::map<std::string, Input::Button::ButtonEvent> _mapButtonEvent;

bool Input::Button::oncePress(std::string const& name, bool const& button)
{
    if (button)
    {
        if (!_mapButtonEvent[name]._noRepeat)
        {
            _mapButtonEvent[name]._noRepeat = true;
            _mapButtonEvent[name]._oncePress = true;
        }
        else
        {
            _mapButtonEvent[name]._oncePress = false;
        }
    }
    else
    {
        _mapButtonEvent[name]._noRepeat = false;
        _mapButtonEvent[name]._oncePress = false;
    }

    return _mapButtonEvent[name]._oncePress;
}
bool Input::Button::oncePress(std::string const& name)
{
    return _mapButtonEvent[name]._oncePress;
}


bool Input::Button::onPress(std::string const& name, bool const& button, int delayToRepeat, int delayRepeat)
{
    _mapButtonEvent[name]._onPress = false;
    _mapButtonEvent[name]._isPress = button;

    if (button)
    {
        if (_mapButtonEvent[name]._tempoToRepeat == 0)
            _mapButtonEvent[name]._onPress = true;

        ++_mapButtonEvent[name]._tempoToRepeat;

        if (_mapButtonEvent[name]._repeat)
        {
            ++_mapButtonEvent[name]._tempoRepeat;
            if(_mapButtonEvent[name]._tempoRepeat > delayRepeat)
            {
                _mapButtonEvent[name]._onPress = true;
                _mapButtonEvent[name]._tempoRepeat = 0;
            }
        }

    }
    else
    {
        _mapButtonEvent[name]._tempoToRepeat = 0;
    }


    if (_mapButtonEvent[name]._tempoToRepeat > delayToRepeat)
    {
        if (delayToRepeat > 0) // if -1 then no repeat
            _mapButtonEvent[name]._repeat = true;
    }
    else
    {
        _mapButtonEvent[name]._repeat = false;
    }

    return _mapButtonEvent[name]._onPress;
}

bool Input::Button::onPress(std::string const& name)
{
    return _mapButtonEvent[name]._onPress;
}

bool Input::Button::isPress(std::string const& name)
{
    return _mapButtonEvent[name]._isPress;
}

void Input::Mouse::update(VAR mouseX, VAR mouseY, int mouseButton)
{
    _x = mouseX;
    _y = mouseY;
    //_isMove = false;
    _button = mouseButton;

    _lastIsClick = _isClick;
    _onClick = false;
    _offClick = false;

    if (_prevX != _x || _prevY != _y)
    {
        (!_isMove) ? _onMove = true : _onMove = false;


        _isMove = true;

        if (_y < _prevY) _moveUP = true;
        if (_y > _prevY) _moveDOWN = true;
        if (_x < _prevX) _moveLEFT = true;
        if (_x > _prevX) _moveRIGHT = true;

        _prevX = _x;
        _prevY = _y;
    }
    else
    {
        (_isMove) ? _offMove = true : _offMove = false;

        _isMove = false;
        _moveUP = false;
        _moveDOWN = false;
        _moveLEFT = false;
        _moveRIGHT = false;
    }

    if (_button && !_down)
    {

        _down = true;
        //log("< Mouse DOWN >");
    }

    if (!_button && !_up)
    {

        _up = true;
        //log("< Mouse UP >");
    }

    if (!_button)
    {
        _drag = false;
        _reSize = false;
        _down = false;
        _isClick = false;
    }
    else
    {
        _isClick = true;
        _up = false;
        //log("< Mouse Pressed >");
    }


    if (_isClick != _lastIsClick)
    {
        _lastIsClick = _isClick;

        if (_isClick)
            _onClick = true;
        else
            _offClick = true;

    }


}
