#ifndef WORLD2D_H_INCLUDED
#define WORLD2D_H_INCLUDED

#include <algorithm>
#include <set>
#include <cmath>
#include "Misc.h"

struct Clip;

template <class TYPE>
using Vector2D = std::vector<std::vector<TYPE>>;

enum
{
	M = -1,
	NW,NE,SW,SE,
	N,S,W,E,
	NM,SM,WM,EM,

	MIDDLE = M, CENTER = M,
	NORTH = N, TOP = N,
	SOUTH = S, BOTTOM = S,
	WEST = W, LEFT = W,
	EAST = E, RIGHT = E,

	NORTH_WEST = NW, TOP_LEFT = NW,
	NORTH_EAST = NE, TOP_RIGHT = NE,
	SOUTH_WEST = SW, BOTTOM_LEFT = SW,
	SOUTH_EAST = SE, BOTTOM_RIGHT = SE,

	MIDDLE_NORTH = NW, TOP_CENTER = NM,
	MIDDLE_SOUTH = SM, BOTTOM_CENTER = SM,
	MIDDLE_WEST = WM, LEFT_CENTER = WM,
	MIDDLE_EAST = EM, RIGHT_CENTER = EM
};


struct Vec2
{
    VAR _x;
    VAR _y;
};

struct Vec3
{
    VAR _x;
    VAR _y;
    VAR _z;
};

struct RGBA
{
    unsigned char _r;
    unsigned char _g;
    unsigned char _b;
    unsigned char _a;
};

struct Line
{
    VAR _x1;
    VAR _y1;
    VAR _x2;
    VAR _y2;

};

struct Circle
{
	VAR _x;
	VAR _y;
	VAR _r;
};

struct Triangle
{
    VAR _x1, _y1;
    VAR _x2, _y2;
    VAR _x3, _y3;
};

struct Rect
{
    VAR _x;
    VAR _y;
    VAR _w;
    VAR _h;

    Rect operator+(Rect const& second)
    {
        Rect rect;
        rect._x = this->_x + second._x;
        rect._y = this->_y + second._y;
        rect._w = this->_w + second._w;
        rect._h = this->_h + second._h;

        return rect;
    }
};

namespace Collide
{
    struct Zone
    {
        unsigned _index;
        Rect _rect; // Rect of the Zone !
        Clip* _clip; // Clip who own this CollideZone
        bool _isCollide = false;
        Zone* _zoneCollideBy = nullptr;
        std::set<Zone*> _vecCollideBy ; // list of other CollideZone who hit this

        Zone(unsigned index, Rect const& rect, Clip* clip = nullptr)
        {
            _index = index;
            _rect = rect;
            _clip = clip;

            _vecCollideBy.clear();
        }
    };

    struct Cell
    {
        std::vector<Zone*> _vecCollideZoneInCell; // Contain all index of CollideZone in the Cell
    };
}


struct Tile
{
    int _id;
    int _type;
    Rect _rect; // Tile Rect
    int _isCollidable;

    std::map<int, int> _mapProperty;

    Tile (int id = 0, int type = 0, Rect rect = {0,0,0,0}, int isCollidable = 0):
        _id(id),
        _type(type),
        _rect(rect),
        _isCollidable(isCollidable)
    {
    }
};

template <class OBJECT>
struct Map2D
{
    unsigned _mapW;
    unsigned _mapH;

    Vector2D<OBJECT*> _vecObject2D;

    Map2D(unsigned mapW, unsigned mapH, OBJECT* object=nullptr):
        _mapW(mapW),
        _mapH(mapH)
    {
        resizeVecObject2D(_mapW, _mapH);

        fillObject2D(object);
    }

    virtual ~Map2D()
    {
        killAll();
    }

    void resizeVecObject2D(unsigned mapW, unsigned mapH)
    {
        _mapW = mapW;
        _mapH = mapH;

        //resizeVec<OBJECT>(_vecObject2D,_mapW);
        _vecObject2D.resize(_mapW);
        for (unsigned x = 0; x < _mapW; ++x)
        {
            resizeVecPtr<OBJECT>(_vecObject2D[x],_mapH);
            for (unsigned y = 0; y < _mapH; ++y)
            {
                _vecObject2D[x][y] = nullptr;
            }
        }
    }

    void killAll()
    {
        for (unsigned x = 0; x < _mapW; ++x)
        {
            for (unsigned y = 0; y < _mapH; ++y)
            {
                if (_vecObject2D[x][y] != nullptr)
                {
                    //std::cout << "delete at : " << x << " , " << y << " address = "<< _vecOject2D[x][y]<<  " \n";
                    delete _vecObject2D[x][y];
                    _vecObject2D[x][y] = nullptr;

                }
            }
            _vecObject2D[x].clear();
        }
        _vecObject2D.clear();
    }

    void fillObject2D(OBJECT* object)
    {
        for (unsigned x = 0; x < _mapW; ++x)
        {
            for (unsigned y = 0; y < _mapH; ++y)
            {
                put(x, y, new OBJECT(*object));
            }
        }
    }

    OBJECT *get(unsigned x, unsigned y)
    {
        if (x < 0 || x > _mapW-1 ||
            y < 0 || y > _mapH-1)
            return nullptr;
        else
            return _vecObject2D[x][y];
    }

    void put(unsigned x, unsigned y, OBJECT *object)
    {
        if (x < 0 || x > _mapW-1 ||
            y < 0 || y > _mapH-1)
            return;
//        OBJECT* tmp = new OBJECT(_vecObject2D[x][y])
        if (nullptr != _vecObject2D[x][y])
            delete _vecObject2D[x][y];

        _vecObject2D[x][y] = object;
        //_vecObject2D[x][y] = new OBJECT(*object);
    }


};


#endif // WORLD2D_H_INCLUDED
