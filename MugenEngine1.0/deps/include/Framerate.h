//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#ifndef FRAMERATE_H
#define FRAMERATE_H

#include <cmath>
#include <allegro5/allegro.h>

class Framerate
{
    public:
        Framerate(ALLEGRO_EVENT_QUEUE* eventQueue, double framerate);
        virtual ~Framerate();

        // Framerate
        static int getFramerate();
        static void pollFramerate();

        ALLEGRO_EVENT_QUEUE* eventQueue();

        static int _nbFrame;
        static int _nbFps;
        static long double _gameTime;
        static long double _oldTime;

    protected:

    private:
        float _prevTime = 0.0f;
        float _restTime = 0.0f;
        float _currentTime = 0.0f;
        float _deltaTime = 0.0f;

        // Framerate control
        double _framerate = 0.0f;


        ALLEGRO_TIMER* _framerateTimer;

        // Direct FPS
//        double _oldTime = 0;
//        double _newTime = 0;
//        double _deltaTime =0;
//        int _fps = 0;
//        int _currentFps = 0;
//        double _averageFPS = 0;
//        std::vector<double> _vecFps;

};

#endif // FRAMERATE_H
