#ifndef IPLAYABLE_H
#define IPLAYABLE_H

#include <memory>
#include <set>

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

#include "Window.h"
#include "Animation.h"
#include "Input.h"

#define GOTO_NEXT_FRAME() _THIS->nextFrame()
#define GOTO_PREV_FRAME() _THIS->prevFrame()
#define ON_FRAME(f) if(_THIS->onFrame(f))

#define ON_PLAY() if(_THIS->isPlay())
#define PLAY_AT(f) _THIS->playAt(f)
#define STOP_AT(f) _THIS->stopAt(f)

#define PLAY() _THIS->play()
#define STOP() _THIS->stop()
#define START() _THIS->start()
#define PAUSE() _THIS->pause()
#define RESUME() _THIS->resume()

#define SHOW() _THIS->show()
#define HIDE() _THIS->hide()

#define _ID _THIS->id()
#define _PARENT _THIS->parent()

// Relative position
#define _X _THIS->_x
#define _Y _THIS->_y
#define _Z _THIS->_z
#define _RECT _THIS->_rect

// Absolute Position
#define _ABSX _THIS->absX()
#define _ABSY _THIS->absY()
#define _OX _THIS->_oX
#define _OY _THIS->_oY
#define _ABSRECT Rect{_ABSX-_OX,_ABSY-_OY,_RECT._w,_RECT._h}

#define _WINDOW _THIS->window()
#define _FONT _THIS->_font
#define _NAME _THIS->_name.c_str()
#define _CURFRAME _THIS->_currentFrame


// ON => Trigger , true one time : Instant
// IS => Status  , true all the time : Period

enum FunctionCondition
{
    ON_PRESS = 1,
    ON_RELEASE,
    ON_FOCUS, IS_FOCUS,
    ON_OVER, IS_OVER,
    ON_OUT, IS_OUT
};


#endif // IPLAYABLE_H
