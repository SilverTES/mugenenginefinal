//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#include <iostream>

template <class M, class E>
E mlog(M msg, E error)
{
#ifdef SHOW_LOG
    std::cout << msg;
#endif // SHOW_LOG
    return error;
}

template <class OBJECT>
void resizeVecPtr(std::vector<OBJECT*>& vecObject, unsigned newSize)
{
    if (newSize == vecObject.size()) // nothing to resize
        return;

    if (newSize < vecObject.size()) // reduce size
    {
        for (unsigned i=0; i<newSize -1; ++i)
        {
            delete vecObject.back();
            vecObject.pop_back();
        }

        vecObject.resize(newSize);
    }
    else  // extend size
    {
        vecObject.resize(newSize, new OBJECT());
    }

}
