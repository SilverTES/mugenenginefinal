#ifndef BASECOMPONENT_H_INCLUDED
#define BASECOMPONENT_H_INCLUDED

#include "Clip.h"
#include "Collision2D.h"
#include "Player.h"
#include "Asset.h"
#include "File.h"

namespace Component
{

    struct ComponentCounter {};

    #define SKIN get<Component::Skin>()
    #define _SKIN _THIS->get<Component::Skin>()
    struct Skin : public Component::Helper<Skin> // Get/Set Player controller button
    {
        int _id = -1;
        RGBA _color = {255,255,255,255};

        VAR _tileW = 8;
        VAR _tileH = 8;

        Clip* setId(int id);
        Clip* setColor(RGBA const& color);
        Clip* setRGB(unsigned char r, unsigned char g, unsigned char b);
        Clip* setAlpha(unsigned char alpha);
        Clip* setTileSize(VAR tileW, VAR tileH);

    };

    #define GUI get<Component::Gui>()
    #define _GUI _THIS->get<Component::Gui>()
    struct Gui : public Component::Helper<Gui> // Get/Set Player controller button
    {
        bool _focusable = true; // focusable by default !
        bool _onPress = false; // trigger : on pressed gui
        bool _isPress = false; // status : is pressed gui
        bool _isFocus = false; // status : is focused gui

        //Clip* setNavigate(Component::Navigate* navigate);
    };

    #define COMMAND get<Component::Command>()
    #define _COMMAND _THIS->get<Component::Command>()
    struct Command : public Component::Helper<Command> // Get/Set Player controller button
    {
        Player* _player = nullptr;

        ~Command();

        Clip* setPlayer(Player* player);
        Clip* setKeyState(ALLEGRO_KEYBOARD_STATE* keyState);
        Clip* setButton(int id, int idKey, int idJoy, int idStick, int idAxis, float idDirection, int idButton);
        Clip* setButton(int id, std::shared_ptr<Button> button);
        Clip* loadController(Json const& controller);
        int onButton(int id);
    };

    #define LINE get<Component::Line>()
    #define _LINE _THIS->get<Component::Line>()
    struct Line : public Component::Helper<Line> //
    {
        VAR _x1 = 0;
        VAR _y1 = 0;
        VAR _x2 = 0;
        VAR _y2 = 0;
        Draw::Color _color;
        VAR _thickness = 0;

        Clip* setLine(VAR x1, VAR y1, VAR x2, VAR y2, Draw::Color color, VAR thickness);
    };

    #define LOOP get<Component::Loop>()
    #define _LOOP _THIS->get<Component::Loop>()
    struct Loop : public Component::Helper<Loop> // Get/Set Loop for animation, ping-pong, repeat, ...
    {
        enum LOOP_TYPE
        {
            LOOP_ONCE,
            LOOP_REPEAT,
            LOOP_PINGPONG
        };

        bool _run = false;
        bool _atEnd = false;
        bool _atBegin = false;
        int _current = 0;   // current position
        int _begin = 0;     // begin at
        int _end = 0;       // end at
        int _direction = 0; // direction of the loop
        int _loopType = 0;

        Clip* setLoop(int current, int begin, int end, int direction, int loopType);
        Clip* start();
        Clip* stop();

        void update();
    };

    #define VELOCITY get<Component::Velocity>()
    #define _VELOCITY _THIS->get<Component::Velocity>()
    struct Velocity : public Component::Helper<Velocity>
    {
        VAR _vecX = 0;
        VAR _vecY = 0;

        VAR _vx = 0;
        VAR _vy = 0;
        VAR _vz = 0;

        VAR _vxMax = 0;
        VAR _vyMax = 0;
        VAR _vzMax = 0;

        VAR _ax = 0;
        VAR _ay = 0;
        VAR _az = 0;

        VAR _vax = 0;
        VAR _vay = 0;
        VAR _vaz = 0;

        Clip* setVecX(VAR vecX);
        Clip* setVecY(VAR vecY);

        Clip* setVX(VAR vx);
        Clip* setVY(VAR vy);
        Clip* setVZ(VAR vz);

        Clip* setVXMax(VAR vxMax);
        Clip* setVYMax(VAR vyMax);
        Clip* setVZMax(VAR vzMax);

        Clip* setAX(VAR ax);
        Clip* setAY(VAR ay);
        Clip* setAZ(VAR az);

        Clip* setVAX(VAR vax);
        Clip* setVAY(VAR vay);
        Clip* setVAZ(VAR vaz);

    } __attribute__((packed));

    #define TEMPO get<Component::Tempo>()
    #define _TEMPO _THIS->get<Component::Tempo>()
    struct Tempo : public Component::Helper<Tempo>
    {
        struct Tic
        {
            bool _isCount = false;
            bool _tic = false;
            int _currentTempo = 0;
            int _duration = 0;
        };

        std::unordered_map<std::string, Tic*> _mapTempo;

        virtual ~Tempo()
        {
            auto it = _mapTempo.begin();
            while (it != _mapTempo.end())
            {
                if (nullptr != it->second)
                {
                    delete it->second;
                    it->second = nullptr;
                }
                ++it;
            }
            _mapTempo.clear();
        }

        Clip* setTempo(std::string name, int duration);
        Clip* start(std::string name, int duration = 0);
        Clip* startAt(std::string name, int tempo = 0);
        Clip* pause(std::string name);
        Clip* stop(std::string name);
        bool iscount(std::string name);
        bool tic(std::string name);
        int current(std::string name);
        int duration(std::string name);

        void update();
    };

    #define ANIMATE get<Component::Animate>()
    #define _ANIMATE _THIS->get<Component::Animate>()
    struct Animate : public Component::Helper<Animate>
    {
        bool _atEnd = false;

        int _currentFrame = 0;

        int _rangeA = 0;
        int _rangeB = 0;

        int _direction = 0;

        int _countPlayDelay = 0;
        int _playDelay = 0;

        int _countFrameDelay = 0;
        int _frameDelay = 0;

        bool _isPlay = false;

        Animation* _animation = nullptr;
        Sequence* _sequence = nullptr;

        Clip* setAnimation(Animation* animation)
        {
            _animation = animation;
            return _clip;
        }

        Clip* setSequence(Sequence* sequence)
        {
            _sequence = sequence;
            return _clip;
        }

        Clip* startAt(int frame, int playDelay = 0, int direction = 1,int rangeA = 0, int rangeB = 0)
        {
            _isPlay = true;
            _playDelay = playDelay;
            _countPlayDelay = 0;
            _countFrameDelay = 0;

            _rangeA = rangeA;
            if (0 == rangeB)
                _rangeB = _sequence->nbFrame()-1;
            else
                _rangeB = rangeB;


            _currentFrame = frame;
            _direction = direction;

            return _clip;
        }

        void stop()
        {
            _isPlay = false;
        }

        void update();

        // draw Animation in specific position of indexed Sequence
        void render(unsigned index, int x, int y)
        {
            _animation->render(index, x, y, _currentFrame);
        }

        // draw Animation in specific position of current Sequance
        void render(int x, int y)
        {
            if (nullptr != _sequence)
                _animation->render(_sequence, x, y, _currentFrame);
        }

        // draw Animtion at Clip Axe Position  of current Sequence
        void render();



    } __attribute__((packed));

    #define DRAGGABLE get<Component::Draggable>()
    #define _DRAGGABLE _THIS->get<Component::Draggable>()
    struct Draggable : public Component::Helper<Draggable>
    {
        bool _isDrag = false;
        bool _isDragRectClip = true;
        bool _isLimitRect = false;
        float _dragX = 0;
        float _dragY = 0;

        Rect _rectDrag;
        Rect _limitRect;

        Clip* setDragRect(Rect const& dragRect);
        Clip* setDragRectClip(bool const& isDragRectClip);
        Clip* setLimitRect(Rect const& limitRect, bool isLimitRect = true);

        void update();
        void render();
    } __attribute__((packed));

    #define CAMERA get<Component::Camera>()
    #define _CAMERA _THIS->get<Component::Camera>()
    struct Camera : public Component::Helper<Camera>
    {
        bool _isMove = false;
        bool _isLimitZone = false;
        Rect _rectView;
        Rect _rectLimitZone;

        Clip* _mainClip = nullptr;

        Clip* setMainClip(Clip* mainClip);
        Clip* setRectView(Rect const& rectView);
        Clip* setRectLimitZone(Rect const& rectLimitZone);
        Clip* setLimitZone(bool isLimitZone);
        void setPosition(VAR x, VAR y);
        void limitViewZone();
        void moveX(VAR vx);
        void moveY(VAR vy);

        void update();
        void render();

    } __attribute__((packed));

    #define _TILEMAP2D _THIS->get<Component::TileMap2D>()
    #define TILEMAP2D get<Component::TileMap2D>()
    struct TileMap2D : public Component::Helper<TileMap2D>
    {
        bool _mapLoaded = false;
        std::string _jsonFileName = "";
        int _idLayer = -1;
        int _idTileSet = -1;
        std::map<int,std::string> _mapTiledProperty;

        VAR _tileW;
        VAR _tileH;

        Rect _rect; // rect of the TileMap2D
        Rect _rectView;; // rect of the View

        Map2D<Tile>* _map2D = nullptr;
        ALLEGRO_BITMAP* _atlas = nullptr;

        Draw::List _drawList;

        virtual ~TileMap2D()
        {
            if (nullptr != _map2D)
                delete _map2D;
        }

        void setTile(int x, int y, Tile* tile);

        Clip* setup(Rect const& rectView, unsigned mapW = 1 , unsigned mapH = 1 , VAR tileW = 1, VAR tileH = 1, VAR x = 0, VAR y = 0);
        //TileMap2D(Rect const& rectView, unsigned mapW = 1 , unsigned mapH = 1 , VAR tileW = 1, VAR tileH = 1, VAR x = 0, VAR y = 0);

        Clip* loadTiledJSON // Create tiles and their properties in _map2D !
        (
            std::string const& jsonFileName,
            int idLayer,
            int idTileSet,
            std::string const& tileSetName,
            Asset::Manager* asset,
            std::string const& path,
            std::map<int,std::string> mapTiledProperty
        );

        Clip* reLoadTileProperty(); // Reload only tile properties, don't recreate tile in _map2D !

        Clip* setRectView(Rect const& rectView);
        Clip* setMapSize(unsigned mapW, unsigned mapH);
        Clip* setTileSize(VAR tileW, VAR tileH);
        Clip* setAtlas(ALLEGRO_BITMAP* atlas);
        Clip* setPosition(VAR x, VAR y);

        Tile* getTile(int x, int y);

        int getTileProperty(int x, int y, int index);
        void setTileProperty(int x, int y, int index, int value);

        int getTileCollidable(int x, int y);
        int getTileType(int x, int y);

        Rect getTileRect(int x, int y, VAR extend = 0);

        void showGrid(Draw::Color const& color);
        void showInfo(Draw::Font* font, Draw::Color const& color);

        void update();
        void render();

    };// __attribute__((packed));

    #define TRANSITION get<Component::Transition>()
    #define _TRANSITION _THIS->get<Component::Transition>()
    struct Transition : public Component::Helper<Transition>
    {
        enum Event
        {
            IS_BEGIN = 0,
            ON_TRANSITION,
            IS_TRANSITION,
            OFF_TRANSITION,
            IS_END

        };

        int _currentPos = 0;

        bool onPos(int pos);
        Clip* setPos(int pos);
    };

    #define VIEWPORT get<Component::Viewport>()
    #define _VIEWPORT _THIS->get<Component::Viewport>()
    struct Viewport : public Component::Helper<Viewport>
    {
        struct View
        {
            Draw::Bitmap* _renderTarget2D;
            Rect _rect;
        };

        std::map<int, View*> _view;

        ~Viewport();
        View* getView(int viewId);
        Rect getRect(int viewId);
        Clip* setViewport(std::shared_ptr<Window> window, int viewId, Rect const& rect);
        Clip* updateViewport(int viewId, Rect const& rect);
        Clip* beginRenderView(std::shared_ptr<Window> window, int viewId);
        Clip* endRenderView(std::shared_ptr<Window> window, int viewId);
    };

}


#endif // BASECOMPONENT_H_INCLUDED
