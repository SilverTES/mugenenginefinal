//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------

#ifndef WINDOW_H
#define WINDOW_H

#define WINSYSTEM // for Windows specific platform

#include <allegro5/allegro.h>
#include <allegro5/allegro_windows.h>
#include <allegro5/allegro_direct3d.h>

#include "Misc.h"

//------------------------------------------------------------------------------ Class Window
class Window
{
    public:
    // Setup
        Window();
        virtual ~Window();
        int init(const char* name, int useAPI, int screenW, int screenH, int scaleWin, int scaleFull, bool fullScreen, bool vsync, bool smooth);
        int done();

    // Render
        void beginRender();
        void endRender();

    // Manager
        void toggleFullScreen(int scale); // Toggle windowed to FullScreen : 0 = Max Zoom , -1 = Default Zoom
        void switchMonitor(int scale); // Switch monitor : 0 = Max Zoom, -1 Default Zoom
        void setMonitor(int monitor, int scale); // Select the Monitor : O = Max Zoom, -1 Default Zoom
        void setScale(int scale); // set Scale : 0 = Max Zoom, -1 Default Zoom
        void setWindow(ALLEGRO_DISPLAY * display, int adapter,bool isFullScreen, int scale);

        void setViewAtCenter(int scale); // Place the View in the center of Monitor by scale !
        int getMaxScale(); // Calculate max scaling factor !
        void pollMonitor(int adapter);

    // Accessor
        bool isFullScreen() const;
        ALLEGRO_DISPLAY * display();
        ALLEGRO_BITMAP * buffer();


        int screenW() const;
        int screenH() const;
        int centerX() const;
        int centerY() const;

        int scaleWin() const;
        int scaleFull() const;
        int viewX() const;
        int viewY() const;
        int viewW() const;
        int viewH() const;

        int currentMonitor(ALLEGRO_DISPLAY * display); // Find the current monitor where the window is !
        int currentMonitor() const; // return the current Monitor selected !
        int currentMonitorX() const;
        int currentMonitorY() const;
        int currentMonitorW() const;
        int currentMonitorH() const;

        // Mouse
        void getMouse(ALLEGRO_MOUSE_STATE * mouseState, float &xMouse, float &yMouse);


    // Debug
        int x() const;
        int y() const;

    //------------------------------------------------------------------------------ Debug
        static void showVideoAdapters();

    protected:
        const char* _name = "";

        // Graphic API used
        int _useAPI = 0; // 0 = OpenGL / 1 = DIRECTX

        // Screen of Game
        int _screenW = 640;
        int _screenH = 360;

        // Windowed
        int _scaleWin = 0;
        int _x = 0;
        int _y = 0;

        // FullScreen
        int _scaleFull = 0;
        int _viewW = 0;
        int _viewH = 0;
        int _viewX = 0;
        int _viewY = 0;

        // Monitor
        int _currentMonitor = 0;
        int _currentMonitorX = 0;
        int _currentMonitorY = 0;
        int _currentMonitorW = 0;
        int _currentMonitorH = 0;

        // View state
        bool _isFullScreen = false;
        bool _isMaxScale = false;
        bool _isVsync = false;
        bool _isSmooth = false;

    private:
        ALLEGRO_DISPLAY *_windowDisplay = NULL;
        ALLEGRO_BITMAP *_windowBuffer = NULL;
};


#endif // WINDOW_H
