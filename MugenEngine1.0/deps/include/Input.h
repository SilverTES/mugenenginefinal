#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

#include "Misc.h"

namespace Input
{
    // Manage button delay/tempo
    struct Button
    {
        struct ButtonEvent
        {
            bool _noRepeat = false; // noRepeat button
            bool _oncePress = false; // button once pressed no delay;
            bool _repeat = false;  // repeat the button
            bool _onPress = false; // button on pressed with delay
            bool _isPress = false; // button is pressed no delay
            int _tempoToRepeat = 0; // tempo before repeat button
            int _tempoRepeat = 0;   // tempo for repeat button
        };

        static bool oncePress(std::string const& name, bool const& button);
        static bool oncePress(std::string const& name);
        static bool onPress(std::string const& name, bool const& button, int delayToRepeat = 30, int delayRepeat = 4);
        static bool onPress(std::string const& name);
        static bool isPress(std::string const& name);

    };


    struct Mouse
    {
        // Position
        VAR _x = 0;
        VAR _y = 0;
        VAR _prevX = 0;
        VAR _prevY = 0;

        // Button
        int _button = 0;
        bool _lastIsClick = false;
        bool _isClick = false; // status button is pressed
        bool _onClick = false; // trigger button on Click
        bool _offClick = false; // trigger button off Click

        // Event
        bool _isMove = false; // mouse is move or not !
        bool _onMove = false; // trigger mouse move !
        bool _offMove = false; // trigger mouse stop move !

        bool _moveUP = false; // mouse is move UP or not !
        bool _moveDOWN = false; // mouse is move DOWN or not !
        bool _moveLEFT = false; // mouse is move LEFT or not !
        bool _moveRIGHT = false; // mouse is move RIGHT or not !

        bool _up = false;   // mouse is up or not !
        bool _down = false; // mouse is down or not !

        // Draggable
        bool _drag = false; // mouse is drag or not !
        bool _reSize = false; // mouse is resize or not !

        void update(VAR mouseX, VAR mouseY, int mouseButton);

    };

}



#endif // INPUT_H_INCLUDED
