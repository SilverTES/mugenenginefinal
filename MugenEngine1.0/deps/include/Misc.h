//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#ifndef MISC_H_INCLUDED
#define MISC_H_INCLUDED

#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>
#include <stack>

#define IS(x) if (nullptr!=x)

#ifdef DEBUG
#define ERRLOG(msg,err) {std::cout<<msg; return err;}
#else
#define ERRLOG(mdg,err) {return 0;}
#endif // DEBUG



//------ Macro hack to create auto increment unique ID constant

#define COUNTER_READ_CRUMB( TAG, RANK, ACC ) counter_crumb( TAG(), constant_index< RANK >(), constant_index< ACC >() )
#define COUNTER_READ( TAG ) COUNTER_READ_CRUMB( TAG, 1, COUNTER_READ_CRUMB( TAG, 2, COUNTER_READ_CRUMB( TAG, 4, COUNTER_READ_CRUMB( TAG, 8, \
    COUNTER_READ_CRUMB( TAG, 16, COUNTER_READ_CRUMB( TAG, 32, COUNTER_READ_CRUMB( TAG, 64, COUNTER_READ_CRUMB( TAG, 128, 0 ) ) ) ) ) ) ) )

#define COUNTER_INC( TAG ) \
constant_index< COUNTER_READ( TAG ) + 1 > \
constexpr counter_crumb( TAG, constant_index< ( COUNTER_READ( TAG ) + 1 ) & ~ COUNTER_READ( TAG ) >, \
          					constant_index< ( COUNTER_READ( TAG ) + 1 ) & COUNTER_READ( TAG ) > ) { return {}; }

#define COUNTER_LINK_NAMESPACE( NS ) using NS::counter_crumb;

#include <utility>

template< std::size_t n >
struct constant_index : std::integral_constant< std::size_t, n > {};

template< typename id, std::size_t rank, std::size_t acc >
constexpr constant_index< acc > counter_crumb( id, constant_index< rank >, constant_index< acc > ) { return {}; } // found by ADL via constant_index




//----- For create a static type for any class or struct !
template <typename T>
struct StaticType
{
    static int _type;
};
template <typename T>
int StaticType<T>::_type = 0;

template <typename T>
struct StaticDefine
{
    static bool _define;
};
template <typename T>
bool StaticDefine<T>::_define = false;


//------

using VAR = float;

template <class M = std::string, class E = int>
static E mlog(M msg, E error = 0);

template <class OBJECT>
void resizeVecPtr(std::vector<OBJECT*>& vecObject, unsigned newSize);


namespace Misc
{
    const float Pi = 3.141592654f;
    const float Rad90 = 1.5707963f;

    template <class T>
    void kill(T& ptr)
    {
        if (nullptr != ptr)
        {
            delete ptr;
            ptr = nullptr;
        }
    }

    struct BoolArray
    {
        int _data = 0;

        void set(int index, int value);
        void on(int index);
        void off(int index);
        bool get(int index);
        void flip(int index);
        void reset();
    };

    inline float degToRad(float x)
    {
        return x / 180 * Pi;
    }

    inline float radToDeg(float x)
    {
        return x / Pi * 180;
    }

    template <typename T> int sgn(T val)
    {
        return (T(0) < val) - (val < T(0));
    }

    VAR sign(VAR value);
    int random(int beginRange, int endRange);
    int random(std::string str, std::string delimiter);
    int getRandom(std::string str, std::string delimiter);
    float pourcent(VAR maxValue, VAR value);
    float proportion(VAR maxValue, VAR value, VAR range);
}

#include "Misc.inl"


#endif // MISC_H_INCLUDED
