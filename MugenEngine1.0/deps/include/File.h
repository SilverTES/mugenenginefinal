//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------

#ifndef FILE_H
#define FILE_H

#include <json.hpp> // Library JSON Manipulation !
#include <fstream>

using Json = nlohmann::json;

extern "C" // Standard LUA Library
{
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
}
#include <selene.h> // Wrapper C++ LUA !

#include "Misc.h"

namespace File
{
    // --- JSON File Manager !
    Json loadJson(std::string const filename);
    void saveJson(std::string const filename, Json data);

    // --- LUA method !
    void PrintTable(lua_State *L);

};

#endif // FILE_H
