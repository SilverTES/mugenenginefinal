#ifndef COLLISION2D_H_INCLUDED
#define COLLISION2D_H_INCLUDED

#include "Clip.h"
#include "Collision2DGrid.h"
#include "Collision2DQuadTree.h"

// Macro Collision

#define ON_COLLIDE() if (_THIS->_isCollide && !_THIS->_vecCollideBy.empty())
#define ON_COLLIDE_ZONE(id) if (_THIS->getCollideZone(id)->_isCollide && !_THIS->getCollideZone(id)->_vecCollideBy.empty())

#define _ID_COLLIDE_NAME _THIS->_idCollideName
#define _ID_COLLIDE_INDEX _THIS->_idCollideIndex
#define _ZONE_COLLIDE_BY(id) _THIS->getCollideZone(id)->_zoneCollideBy
#define _ID_ZONE_COLLIDE_BY(id) _THIS->getCollideZone(id)->_zoneCollideBy->_clip->_id


#define ON_COLLIDE_NAME(name) if (Collision2D::onCollideByName(_THIS,name) >= 0)
#define ON_COLLIDE_INDEX(index) if (Collision2D::onCollideByIndex(_THIS,index) >= 0)
#define ON_COLLIDE_TYPE(index) if (Collision2D::onCollideByType(_THIS,index) >= 0)

#define ON_COLLIDE_ZONE_CLIP_NAME(id, name, indexZone) if (Collision2D::onCollideZoneByClipName(_THIS->getCollideZone(id),name,indexZone) != nullptr)
#define ON_COLLIDE_ZONE_CLIP_INDEX(id, index, indexZone) if (Collision2D::onCollideZoneByClipIndex(_THIS->getCollideZone(id),index,indexZone) != nullptr)
#define ON_COLLIDE_ZONE_CLIP_TYPE(id, index, indexZone) if (Collision2D::onCollideZoneByClipType(_THIS->getCollideZone(id),index,indexZone) != nullptr)


namespace Collision2D
{

    Vec2 getReboundVec2D(Vec2 v, Vec2 N); // return Vec2 vector of Rebound Vector v & Normal N
    Vec2 getNormalPointLine(Vec2 A, Vec2 B, Vec2 C); // return Vec2 normal of C on line AB
    Vec2 projectionPointLine(Vec2 A, Vec2 B, Vec2 C); // return Vec2 projection of C on line AB

    // Collision Type
    bool pointRect(Vec2 p, Rect r); // Test Point in Rect
    bool pointCircle(Vec2 p, Circle c); // Test Point in Circle

    bool pointPolygon(Vec2 P, Vec2 pTab[], int nbP); // Test Point in Polygon Convexe
    bool pointPolygonEX(Vec2 P, Vec2 pTab[],int nbP); // Test Point in Polygon Any

    int segmentXSegment(Vec2 A, Vec2 B, Vec2 I, Vec2 P); // Test segment cut segment // recursive for count nb cut
    bool lineSegment(Vec2 A, Vec2 B, Vec2 O, Vec2 P); // Test line hit Segment
    bool segmentSegment(Vec2 A, Vec2 B, Vec2 O, Vec2 P); // Test Segment hit Segment
    bool segmentSegmentEX(Vec2 A, Vec2 B, Vec2 O, Vec2 P); // Test Segment hit Segment
    bool lineCircle(Vec2 A, Vec2 B, Circle C); // Test Circle hit Line
    bool segmentCircle(Vec2 A, Vec2 B, Circle C); // Test Circle hit Segment

    bool circleCircle(Circle const& c1, Circle const& c2); // Test Circle hit Circle
    bool rectRect(Rect const& r1, Rect const& r2); // Test Rect hit Rect

    void resetAllClip(Clip* clip); // Reset all collide in the Clip
    void resetAllZone(Clip* clip); // Reset all collide in the Clip

    void makeCollideClip(Clip* clip1, Clip* clip2); // Set collide on clip1 & clip2
    void addIndexCollideBy(Clip* clip, int id); // Add Id of clip collided, avoid duplication of id
    void makeCollideZone(Collide::Zone* zone1, Collide::Zone* zone2); // Set collide on zone1 & zone2

    int onCollideByName(Clip* clip, std::string const& name); // Get id of clip collided by Name, if not found return -1
    int onCollideByIndex(Clip* clip, int id); // Get id of clip collided by Index, if not found return -1
    int onCollideByType(Clip* clip, int type);

    Collide::Zone* onCollideZoneByClipName(Collide::Zone* zone, std::string const& name, unsigned indexZone); // Get id of clip collided by Name, if not found return nullptr
    Collide::Zone* onCollideZoneByClipIndex(Collide::Zone* zone, int id, unsigned indexZone); // Get id of clip collided by Index, if not found return nullptr
    Collide::Zone* onCollideZoneByClipType(Collide::Zone* zone, int type, unsigned indexZone); // Get type of clip collided by Index, if not found return nullptr


    // Process Collision System Clip*Clip / rectRect
    void bruteSystemClip(Clip* clip);
    void gridSystemClip(Clip* clip, GridSystem* grid);
    void gridSystemZone(Clip* clip, GridSystem* grid);
    void quadTreeSystemClip(Clip* clip, QuadTree* quad);

}


#endif // COLLISION2D_H_INCLUDED
