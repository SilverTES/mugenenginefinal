#ifndef COMPONENT_H_INCLUDED
#define COMPONENT_H_INCLUDED

#include <map>
#include <algorithm>
#include <Misc.h>
//#include "Animation.h"

struct Clip;

namespace Component
{
    extern std::map<std::string, int> _mapType; // extern for avoid multiple definition

    inline int type (std::string componentName, bool createNewComponentType = false)
    {
        // if Name of component don't exist then create a new component name
        // by increase +1 with the highest id of component !
        if (_mapType.find(componentName) == _mapType.end())
        {
            if (createNewComponentType)
            {
                int lastComponent = -1;

                if (!_mapType.empty())
                {
                    // Get the highest element values in the map !
                    auto it = std::max_element(_mapType.begin(), _mapType.end(),
                                               [](const std::pair<std::string, int>& p1, const std::pair<std::string, int>& p2)
                    {
                        return p1.second < p2.second;
                    });

                    lastComponent = it->second;

                }

                _mapType[componentName] = lastComponent+1;
            }
            else
            {
                return 0;
            }
        }
        return _mapType[componentName];
    }

    struct Base
    {
        int _type;
        Clip *_clip = nullptr;

        Base()
        {
            StaticType<Base>::_type = type("BASE",true);
        }

        virtual Base *clone() const = 0;
        virtual ~Base() // Avoid warning when we want to delete Component !
        {

        }
//
//        virtual void update() = 0;
//        virtual void render() = 0;


    } __attribute__((packed));

    template <class DERIVED>
    struct Helper : public Base
    {
        Helper()
        {
            if (!StaticDefine<DERIVED>::_define) // If not defined then define !
            {
                StaticDefine<DERIVED>::_define = true;
                _type = StaticType<DERIVED>::_type = Component::type(typeid(DERIVED).name(), true);
            }
            else
                _type = StaticType<DERIVED>::_type;
        }

        virtual Base *clone() const
        {
            return new DERIVED(static_cast<const DERIVED&>(*this));
        }
    } __attribute__((packed));

}



#endif // COMPONENT_H_INCLUDED
