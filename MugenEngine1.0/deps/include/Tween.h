#ifndef TWEEN_H_INCLUDED
#define TWEEN_H_INCLUDED

#include <cmath>
#include <algorithm>

#define PI 3.14

namespace Tween
{
    float linear(float t, float b, float c, float d);
    float expoEaseIn(float t, float b, float c, float d);
    float expoEaseOut(float t, float b, float c, float d);
    float expoEaseInOut(float t, float b, float c, float d);
    float cubicEaseIn(float t, float b, float c, float d);
    float cubicEaseOut(float t, float b, float c, float d);
    float cubicEaseInOut(float t, float b, float c, float d);
    float quarticEaseIn(float t, float b, float c, float d);
    float quarticEaseOut(float t, float b, float c, float d);
    float quarticEaseInOut(float t, float b, float c, float d);
    float quinticEaseIn(float t, float b, float c, float d);
    float quinticEaseOut(float t, float b, float c, float d);
    float quinticEaseInOut(float t, float b, float c, float d);
    float quadraticEaseIn(float t, float b, float c, float d);
    float quadraticEaseOut(float t, float b, float c, float d);
    float quadraticEaseInOut(float t, float b, float c, float d);
    float sineEaseIn(float t, float b, float c, float d);
    float sineEaseOut(float t, float b, float c, float d);
    float sineEaseInOut(float t, float b, float c, float d);
    float circularEaseIn(float t, float b, float c, float d);
    float circularEaseOut(float t, float b, float c, float d);
    float circularEaseInOut(float t, float b, float c, float d);
    float backEaseIn(float t, float b, float c, float d);
    float backEaseOut(float t, float b, float c, float d);
    float backEaseInOut(float t, float b, float c, float d);
    float elasticEaseIn(float t, float b, float c, float d);
    float elasticEaseOut(float t, float b, float c, float d);
    float elasticEaseInOut(float t, float b, float c, float d);

    float bounceEaseIn(float t,float b , float c, float d);
    float bounceEaseOut(float t,float b , float c, float d);
    float bounceEaseInOut(float t,float b , float c, float d);

};

#endif // TWEEN_H_INCLUDED
