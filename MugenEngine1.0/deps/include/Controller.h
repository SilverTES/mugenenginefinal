//------------------------------------------------------------------------------
//--- MugenEngine
//------------------------------------------------------------------------------
#ifndef CONTROLLER_H_INCLUDED
#define CONTROLLER_H_INCLUDED

#include <memory>
#include <iostream>
#include <map>
#include <functional>

#include <SFML/Window.hpp>

#include "Window.h"
#include "Misc.h"
#include "Input.h"
#include "Player.h"

#define DEAD_ZONE 50

#define KEY_PRESS(key) (al_key_down(&_keyState, key))

int const MAX_JOYSTICK(16);

class Player;
//class PlayerManager;

enum SNESButton
{
    PAD_UNDEFINED = 0,
    PAD_START,
    PAD_SELECT,
    PAD_UP,
    PAD_DOWN,
    PAD_LEFT,
    PAD_RIGHT,
    PAD_A,PAD_B,PAD_X,PAD_Y,
    PAD_L,PAD_R,
    MAX_BUTTONS
};


struct Button
{
    Button(int id = 0, int idKey = 0, int idJoy = 0, int idStick = 0, int idAxis = 0, float idDirection = 0, int idButton = 0):
    _id(id),
    _idKey(idKey),
    _idJoy(idJoy),
    _idStick(idStick),
    _idAxis(idAxis),
    _idDirection(idDirection),
    _idButton(idButton),
    _isPressed(false)
    {
        //log("+++ Button \n");
    }
    virtual ~Button()
    {
        //log("--- Button \n");
    }

    int _id;      // SNESButton enum !
    int _idKey;     // Keyboard Id
    int _idJoy;     // Gamepad Id
    int _idStick;   // Stick of Gamepad Id
    int _idAxis;   // Axis of Stick Id
    float _idDirection; // Direction of the Axis Id
    int _idButton;  // Button of Gamepad Id
    bool _isPressed; // if button is pressed !

};

class Controller
{
    public:
        Controller();

        virtual ~Controller();

        void setKeyState(ALLEGRO_KEYBOARD_STATE* keyState);

        void showController();
        static void showAll();

        void setButton(int id, int idKey, int idJoy, int idStick, int idAxis, float idDirection, int idButton);
        void setButton(int id, std::shared_ptr<Button> button);
        void forceButton(int id, bool pressed);
        int  getButton(int id);
        std::shared_ptr<Button> getButtonSetup(int id);

        static int getNumJoystick();

        void pollJoystick();

        static std::vector<std::string> SNESButtonDico; // Extern variable return string "PAD_XXXX" by id
        static std::map<std::string, int> SNESButtonDicoMap;
        static std::string getSNESButtonDico(unsigned id);

        static int scanKey();
        static void showGamePadInfo (int index);

        static std::shared_ptr<Button> getControllerButton(int id);
//        static void assignButton (PlayerManager &playerManager, int player, int id, std::function<void(int, int)> run); // Fonction bloquante , wait KEY or JOY inputs
        static void assignButton (Player* player, int id, std::function<void(int, int)> run); // Fonction bloquante , wait KEY or JOY inputs

        static void mapButton(Player *mapPlayer, int idButton);
        static void cancelMapButton(Player *mapPlayer, int idButton = 0);

        void pollButton();

        Player *mapPlayer();

        int mapIdButton();

        bool isAssignButton();
        bool isAssignButtonOK();


    protected:
    private:

        bool _isAssignButton = false;    // if Button need to be assigned
        bool _isAssignButtonOK = false;  // if button is assigned
        int _currentAssignIdButton = -1;           // Current button id need to be assigned
        Player *_currentAssignPlayer = nullptr;    // Current player who own the current button to be assigned

        std::shared_ptr<Button> _arrayButton[MAX_BUTTONS] = {};

        ALLEGRO_KEYBOARD_STATE* _keyState = nullptr;
        //ALLEGRO_JOYSTICK_STATE _joyState;
        //ALLEGRO_JOYSTICK * _arrayJoystick[MAX_JOYSTICK] = {NULL};
        int _joystickCount;



};


#endif // CONTROLLER_H_INCLUDED
