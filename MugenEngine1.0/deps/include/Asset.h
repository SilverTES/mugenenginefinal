#ifndef ASSET_H
#define ASSET_H


#include <unordered_map>

#include "IContainer.h"
#include "IPlayable.h"
#include "Draw.h"
#include "Sound.h"
#include "File.h"

#define MAKE_ASSET(name) (new Asset::Manager(name))

#define GET_FONT(x) get<Asset::Font>(x)->_data
#define GET_BITMAP(x) get<Asset::Bitmap>(x)->_data
#define GET_SAMPLE(x) get<Asset::Sample>(x)->_data

static size_t indexAsset = 0;

namespace Asset
{

    struct Data
    {
        int _id = 0;
        std::string _name = "";
        const char *_fileName = "";

        Data()
        {
            _id = indexAsset;
            ++indexAsset;
        }

        virtual ~Data()
        {
            #ifdef SHOW_LOG
            std::cout << "- Delete Data : "<< _name << "\n";
            #endif // SHOW_LOG
        }

        int getId()
        {
            return _id;
        }
    };

    using MapAsset = std::unordered_map<std::string, Data*>;

    struct Bitmap : public Data
    {
        Draw::Bitmap *_data = nullptr;

        Bitmap(std::string name, const char *fileName);
        virtual ~Bitmap();
    };

    struct Font : public Data
    {
        Draw::Font* _data = nullptr;

        Font(std::string name, const char* fileName, int fontSize, int flags);
        virtual ~Font();
    };

    struct Sample : public Data
    {
        Sound::Sample *_data = nullptr;

        Sample(std::string name, const char*fileName);
        virtual ~Sample();
    };

    struct Manager
    {
        std::string _name = "";
        MapAsset _mapAsset;

        Manager(std::string const& name);
        virtual ~Manager();
        Manager *add(Data *content);
        bool del(std::string name);

        template <class C>
        C *get(std::string name)
        {
            auto it = _mapAsset.find(name);

            if (it != _mapAsset.end())
                return static_cast<C*>(it->second);

            return nullptr;
        }

        void showAll();
    };

    Manager* loadJSON(std::string const& fileName);

}

#endif // ASSET_H
