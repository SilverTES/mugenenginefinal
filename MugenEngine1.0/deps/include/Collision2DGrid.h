#ifndef COLLISION2DGRID_H_INCLUDED
#define COLLISION2DGRID_H_INCLUDED

#include "World2D.h"
namespace Collision2D
{
    struct GridSystem
    {
        VAR _originX;
        VAR _originY;

        int _gridW;
        int _gridH;
        int _cellSize;

        std::vector<std::vector<Collide::Cell*>> _vec2dCell;  // Map2D of Cell
        std::vector<Collide::Zone*> _vecCollideZone;   // List of Collide::zone in one Cell : Always Refreshed

        GridSystem(int gridW, int gridH, int cellSize);
        ~GridSystem();

        Collide::Cell *cell(unsigned x, unsigned y);

        void clearAll();

        void insert(unsigned index, Rect const& rect, Clip* clip = nullptr);
        void add(Collide::Zone* collideZone);

        std::vector<Collide::Zone*> findNear(std::vector<Collide::Zone*> &_vecZoneTemp, Rect rect);
        std::vector<Collide::Zone*> findNear
        (
            std::vector<Collide::Zone*> &_vecZoneTemp,
            Collide::Zone* zoneTest
        );

        Collide::Zone* getNearest(Rect rect);

        void setPosition(int x, int y);

        void update();
        void render();

    };
}
#endif // COLLISION2DGRID_H_INCLUDED
