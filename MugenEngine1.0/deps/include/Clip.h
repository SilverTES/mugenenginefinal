#ifndef CLIP_H_INCLUDED
#define CLIP_H_INCLUDED

#include <map>

#include "Misc.h"
#include "Draw.h"
#include "Input.h"
#include "IContainer.h"
#include "IPlayable.h"

#include "Component.h"

//#define MAKE_CLIP(name,...) (new Clip(name,##__VA_ARGS__))
#define _ORIGINAL _THIS->_original
#define _MASTER _THIS->_master

#define MAKE_CLIP(name) (new Clip(name))
#define MAKE_CLONE(original,cloneName) (Clip::cloneOf(original,cloneName))->appendTo(original->_parent)
#define MAKE_CLONE_ALONE(original,cloneName) (Clip::cloneOf(original,cloneName))
#define KILL_THIS() _PARENT->del(_ID);return

#define CODE [&](Clip* _THIS)
#define UPDATE(code) setUpdate([&](Clip* _THIS){code})
#define RENDER(code) setRender([&](Clip* _THIS){code})
#define FUNCTION(condition, code) setFunction(condition, [&](Clip* _THIS){code})
#define RUN_FUNCTION(condition) if (nullptr != _THIS->_mapFunction[condition]) _THIS->runMapFunction(condition,_THIS);

#define INIT(code) setInit([&](Clip * _THIS){code})
#define DONE(code) setDone([&](Clip * _THIS){code})

#define _NUMBER(name) _THIS->_mapNumber[name]
#define _STRING(name) _THIS->_mapString[name]
#define _CLIP(name) _THIS->_mapClip[name]

//#define DRAW_TEXTF(color,x,y,flag,text,...) if (_FONT!=nullptr) al_draw_textf(_FONT,color,x,y,flag,text,##__VA_ARGS__)
//#define DRAW_TEXT(color,x,y,flag,text) if (_FONT!=nullptr) al_draw_text(_FONT,color,x,y,flag,text)


struct Message;

struct Clip : public IContainer<Clip>
{
    std::string _name;

    Clip *_parent = nullptr;
    bool _isActive = false;  // if Clip is active
    bool _isPlay = true;    // if Clip is playing
    bool _isVisible = true; // if Clip is visible
    bool _isAppend = false; // If append to an Parent Clip !

    // Collision Check
    bool _isCollide = false;
    bool _isCollidable = false;
    int _idCollideName = -1;
    int _idCollideIndex = -1;
    std::set<int> _vecCollideBy; // std::set for avoid duplicate elements

    //Mouse
    bool _isOver = false;  // Mouse over the Clip
    bool _isOut = false;   // Mouse out of Clip
    bool _isFocus = false; // Clip is Focused
    bool _isClick = false; // Clip is Clicked

    //
    int _type = -1; // Type of Clip ex: Root = 0, Camera = 1, Player = 2, ... -1 = nothing
    int _id = 0;   // Index of Clip
    int _currentFrame = 0;
    int _nbFrame = 0;

    // World Position
    VAR _x = 0;
    VAR _y = 0;
    VAR _z = 0;

    VAR _cameraMoveFactor = 0;

    // Rect of the Clip = Draw position
    Rect _rect = Rect{0,0,0,0};

    // Offset : _rect._x, _rect._y <-> _x, _y
    VAR _oX = 0;
    VAR _oY = 0;



//    ALLEGRO_FONT* _font = nullptr;
//    std::shared_ptr<Window> _window;
    Input::Mouse* _mouse = nullptr;

    std::shared_ptr<std::function<void(Clip*)>> _update = nullptr;
    std::shared_ptr<std::function<void(Clip*)>> _render = nullptr;

    std::shared_ptr<std::function<void(Clip*)>> _init = nullptr;
    std::shared_ptr<std::function<void(Clip*)>> _done = nullptr;

    std::map<int, std::shared_ptr<std::function<void(Clip*)>>> _mapFunction;

    Message* _message = nullptr;

    using Pool = std::map<std::string, Clip*>;

    Clip* _master = nullptr;
    Clip* _original = nullptr;
    static Clip* getMaster(Clip* clip);

    static bool _showClipInfo;

    std::map<int, Component::Base*> _mapComponent;

    // Dyamic additional variable for the Clip !
    std::map<std::string, int> _mapNumber;
    std::map<std::string, std::string> _mapString;
    std::map<std::string, Clip*> _mapClip;

    // map of Collide Zone(Rect) of Clip
    std::map<int, Collide::Zone*> _mapCollideZone;

    Clip* setFunction(int functionId, std::function<void(Clip*)> const& functionDef);
    Clip* setUpdate(std::function<void(Clip*)> const& update);
    Clip* setRender(std::function<void(Clip*)> const& render);
    Clip* setInit(std::function<void(Clip*)> const& init);
    Clip* setDone(std::function<void(Clip*)> const& done);

    void runMapFunction(int functionId, Clip* playable);
    void runUpdate(Clip* playable);
    void runRender(Clip* playable);
    void runInit(Clip* playable);
    void runDone(Clip* playable);


    Clip* setCollidable(bool isCollidable);
    Clip* setCameraMoveFactor(float cameraMoveFactor);
    Clip* setSize(int w, int h);
    Clip* setActive(bool isActive);
    Clip* setVisible(bool isVisible);
    Clip* setMouse(Input::Mouse* mouse);
    Clip* setId(int id);

    int id();
    std::string name() const;
    Clip* setType(int type);
    int type();
    void hide();
    void show();

    // Player
    bool isPlay() const;
    void play();
    void stop();
    void pause();
    void resume();
    void start();
    void playAt(int frame);
    void stopAt(int frame);
    bool onFrame(int frame);
    void prevFrame();
    void nextFrame();
    Rect absRect();
    VAR absX();
    VAR absY();
    VAR parentX();
    VAR parentY();
    VAR parentRectX();
    VAR parentRectY();
    VAR parentRectW();
    VAR parentRectH();

//    template <class COMPONENT>
//    COMPONENT* get()
//    {
//        COMPONENT component;
//        return static_cast<COMPONENT*>(_mapComponent[component._type]);
//    }!

    template <class COMPONENT>
    COMPONENT* get()
    {
        return static_cast<COMPONENT*>(_mapComponent[StaticType<COMPONENT>::_type]);
    }

    template <class COMPONENT>
    Clip* attach()
    {
        std::cout << "Create Component : " << StaticType<COMPONENT>::_type << "\n";
        COMPONENT* component = new COMPONENT();
        _mapComponent[StaticType<COMPONENT>::_type] = component;

        std::cout << "Component created OK !\n";
        return this;
    }

    template <class COMPONENT>
    Clip* detach()
    {
        if (nullptr != _mapComponent[StaticType<COMPONENT>::_type])
            delete _mapComponent[StaticType<COMPONENT>::_type];

        _mapComponent.erase(StaticType<COMPONENT>::_type);

        return this;
    }

    Clip* setNumber(std::string const& name, int const& value);
    Clip* setString(std::string const& name, std::string const& value);
    Clip* setClip(std::string const& name, Clip* clip);

    void updateClipRect();
    void update();
    void render();

    static void showClipInfo();
    static void hideClipInfo();

    static Clip* cloneOf(Clip* original, std::string name = "");

//    Clip* setup(
//        std::shared_ptr<Window> window,
//        ALLEGRO_FONT* font,
//        Input::Mouse* mouse,
//        bool isActive = false);

    Clip(std::string const& name = "");
    virtual ~Clip();

    Clip* setPosition(int x = 0, int y = 0, int z = 0);
    Clip* parent();
    Clip* setParent(Clip *parent);  // Set a parent
    Clip* appendTo(Clip *parent);   // append to Parent
//    Clip* setupParent();            // Inherite Parent main datas
    Clip* moveTo(Clip *newParent);  // Experimental

    Clip* attach(Component::Base* component);
    Clip* detach(int type);

    Clip* setX(VAR x);
    Clip* setY(VAR y);
    Clip* setZ(VAR z);
    Clip* setPivotX(VAR x);
    Clip* setPivotY(VAR y);
    Clip* setPivot(VAR x, VAR y);
    Clip* setPivot(int pivot);

    Clip* setCollideZone(int index, Rect const& rect);
    Collide::Zone* getCollideZone(int index);
    Clip* updateCollideZone(int index, Rect const& rect);
    Clip* renderCollideZone(int index, Draw::Color const& color);


    Clip* showRect(Draw::Color const& color);
    Clip* showPivot(Draw::Color const& color, float r);
    Clip* showInfo(Draw::Font* font, Draw::Color const& color);
    Clip* showComponent(Draw::Font* font, VAR x, VAR y, Draw::Color const& color);
};

#endif // CLIP_H_INCLUDED
