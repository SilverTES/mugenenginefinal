#ifndef SOUND_H_INCLUDED
#define SOUND_H_INCLUDED

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

namespace Sound
{
    using Sample = ALLEGRO_SAMPLE;
}

#endif // SOUND_H_INCLUDED
