#ifndef DRAW_H_INCLUDED
#define DRAW_H_INCLUDED

#include <vector>
#include <memory>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_opengl.h>

#include "World2D.h"

namespace Draw
{
    using Color = ALLEGRO_COLOR;
    using Bitmap = ALLEGRO_BITMAP;
    using Font = ALLEGRO_FONT;
    using List = std::vector<ALLEGRO_VERTEX>;

    // Primitives
    void lineBegin(List& drawList);
    void lineEnd(List& drawList);
    void lineBatch(List& drawList, VAR x1, VAR y1, VAR x2, VAR y2, Color const& color);

    void line(VAR x1, VAR y1, VAR x2, VAR y2, Color const& color, VAR thickness);
    void lineAA(VAR x1, VAR y1, VAR x2, VAR y2, RGBA const& color, VAR thickness);

    void sight(List& drawList, VAR x, VAR y, int screenW, int screenH, Color const& color);
    void grid(VAR x, VAR y, VAR w, VAR h, int sizeX, int sizeY, Color const& color);
    void gridBatch(List& drawList, VAR x, VAR y, VAR w, VAR h, int sizeX, int sizeY, Color const& color);
    void grid(Rect const& rect, int sizeX, int sizeY, Color const& color);
    void gridBatch(List& drawList, Rect const& rect, int sizeX, int sizeY, Color const& color);
    void losange(List& drawList, VAR x, VAR y, VAR w, VAR h, Color const& color);


    void triangle(Triangle triangle, Color color, VAR thickness);
    void triangleFill(Triangle triangle, Color color);
    void rect(Rect rect, Color color, VAR thickness);
    void rectFill(Rect rect, Color color);
    void roundRect(Rect rect, VAR rx, VAR ry, Color color, VAR thickness);
    void roundRectFill(Rect rect, VAR rx, VAR ry, Color color);
    void circle(Circle circle, Color color, VAR thickness);
    void circleFill(Circle circle, Color color);

    // Bitmap

    void bitmapBegin(List& drawList);
    void bitmapEnd(List& drawList, Bitmap* atlas);
    void bitmapBatch(List& drawList, Rect rectSrc, Rect rectDest, Color col = al_map_rgba(255, 255, 255, 255));

    void mosaic(Rect rectView, VAR x, VAR y, int repX, int repY, ALLEGRO_BITMAP* bitmap, Color const& color);
    void mosaic(Rect rectView, VAR x, VAR y, int repX, int repY, ALLEGRO_BITMAP* bitmap, VAR tileX, VAR tileY, VAR tileW, VAR tileH, ALLEGRO_COLOR const& color);
    void mosaicBatch(List& drawList, Rect rectView, VAR x, VAR y, int repX, int repY, ALLEGRO_BITMAP* bitmap, ALLEGRO_COLOR const& color);

    // Shader

    ALLEGRO_SHADER* createShader(std::string const& fileNameVert, std::string const& fileNameFrag);
}
#endif // DRAW_H_INCLUDED
